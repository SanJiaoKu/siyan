﻿<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

Route::get('abc', 'index/LoginReg/abc');
// 根目录
Route::get('/', 'index/Index/index');
// 登录页面
//Route::get('login', 'index/LoginReg/index');
// 登录操作
Route::get('login/$', 'index/LoginReg/login');
Route::get('login/:syuid$', 'index/LoginReg/loginCode');
// 退出登录
Route::any('logout', 'index/Login/logout');
// index控制器
// 首页Banner图
Route::get('index_banner', 'index/Index/indexBannerList');
// 首页Theme名称列表
Route::get('index_theme', 'index/Index/themeList');
Route::get('theme_product', 'index/Index/theme_product');
// 商品列表页面
Route::get('product', 'index/Product/index');
// 商品列表页面json
Route::get('product/theme_page', 'index/Product/themeProductJson');
// 商品详情页面
Route::get('product/info', 'index/Product/info');
// 加载商品 size 的方法
Route::post('product_size', 'index/Product/size');
// banner 跳转到 商品或者专题
Route::get('banner/to', 'index/Product/toProductTheme');
// 商品分类页面
Route::get('category', 'index/Category/index');
Route::any('category/getCategoryInfo', 'index/Category/categoryInfo');
// 专题页面
Route::get('theme', 'index/Theme/index');
// 购物车页面

// 加入购物车
Route::post('addToCart', 'index/Cart/addToCart');
// 购物车商品的增加/减少
Route::post('addOrReduce', 'index/Cart/addOrReduce');
// 全选或者全不选
Route::post('changeAll', 'index/Cart/changeAll');
// 改变选中状态
Route::post('changSelect', 'index/Cart/changSelect');
// 删除一条数据
Route::post('delProduct', 'index/Cart/delCartProduct');
// 微信openid
Route::get('wx_auth', 'index/WeChat/Authentication');
// 注册
Route::get('reg/$', 'index/LoginReg/reg_view');
Route::get('reg/:code$', 'index/LoginReg/regCode');
Route::post('do_reg', 'index/LoginReg/reg');
Route::post('check_username', 'index/LoginReg/check_name');
Route::post('check_mobile', 'index/LoginReg/check_mobile');
// 个人中心
Route::get('myself', 'index/Myself/index');
Route::get('myself/info', 'index/Myself/info');
Route::post('myself/update', 'index/Myself/update');
Route::get('myself/address', 'index/Myself/address');
Route::get('myself/message', 'index/Myself/message');
Route::get('myself/downinfo', 'index/Myself/down');
Route::get('myself/subUsers', 'index/Myself/subUsers');
Route::get('mycode', 'index/Myself/mycode');
// 新增收货地址
Route::post('myself/insertAddress', 'index/Myself/insertAddress');
// 编辑收货地址
Route::get('myself/delAddr', 'index/Myself/delAddr');
Route::get('myself/changeAddress', 'index/Myself/changeAddress');
Route::get('myself/detail', 'index/Myself/detail');
Route::post('myself/editAddress', 'index/Myself/editAddress');
// 帮助中心
Route::get('myself/help', 'index/Myself/help');
// 物流查询
Route::get('myself/transportation', 'index/Myself/transportation');
//// 待支付
//Route::get('order/un_pay', 'index/Order/un_pay');
//Route::get('un_pay_json', 'index/Order/un_pay_json');
//Route::get('order/pay', 'index/Order/already_pay');
//// 配送中
//Route::get('order/to_address', 'index/Order/to_address');
//// 已收货
//Route::get('order/already_receive', 'index/Order/already_receive');
// 售后服务
Route::get('myself/afterSale', 'index/Myself/afterSale');
// 优惠券
Route::get('myself/Coupon', 'index/Myself/Coupon');
// 赠送优惠券
Route::post('myself/togive', 'index/Myself/togive');
// 订单
//Route::get('order', 'index/Order/index');
// 确认支付信息
Route::get('payment', 'index/Cart/payment');
// 支付
Route::post('pay', 'index/WeChat/is_pay');
// 单一支付回调
Route::post('payone', 'index/WeChat/pay');
// 支付会回调
Route::post('call', 'index/WeChat/call');
Route::post('member_call', 'index/WeChat/member_call');

Route::get('code_reg', 'index/LoginReg/code_reg');
Route::get('member', 'index/Myself/member');
Route::post('become_member', 'index/Myself/become_member');
//Route::get('/status_ok/:gid', 'index/Myself/status_ok');

// sale_list
Route::get('sale_list', 'index/Sale/getList');
Route::get('sale_index_list', 'index/Index/getSaleList');
// 农副产品_list
Route::get('deputy_list', 'index/Deputy/getList');
// 手机数码_list
// Route::get('digital_list', 'index/Digital/getList');
// 手机数码_list
// Route::get('digital_list', 'index/Digital/getList');
Route::get('digital_list', 'index/Index/getDigitalList');
// 品牌美状_list
Route::get('beauty_list', 'index/Beauty/getList');
// 品牌服装_list
Route::get('clothing_list', 'index/Clothing/getList');
// 生活超市_list
Route::get('supermarket_list', 'index/Supermarket/getList');
// 本地预售_list
Route::get('presale_list', 'index/Presale/getList');
// 想你所想_list
Route::get('missyou_list', 'index/Missyou/getList');
// 删除订单
Route::get('order/delete', 'index/Order/delete');
// 立即购买
Route::get('buy/:gid', 'index/Myself/buy');

Route::any('test', 'index/Test/index');
// 单件购买回调index/Myself/b
Route::get('/orderGeneration', 'index/order/orderGeneration');
//Route::get('/express', 'index/express/index');

Route::get('/myself/sign', 'index/signed/index');
Route::get('/myself/integral', 'index/signed/read');
Route::get('/myself/integral/:limit', 'index/signed/read');


Route::get('/myself/integral', 'index/signed/read');


Route::get('/integral/:limit', 'index/signed/read');
Route::get('tix_list', 'index/Myself/tix');
Route::post('price_tixian', 'index/Tixian/index');
// 等级
Route::get('/sign/grade', 'index/signed/grade');

Route::get('/reflect', 'index/reflect/index');


Route::get('/activity_login', 'index/cutDown/activity_login');

Route::get('/activity_create', 'index/cutDown/activity_create');
Route::post('/activity_create', 'index/cutDown/activity_create');

Route::get('/cut/:cid/:uid', 'index/CutDown/index');

Route::get('/cutdown/:oid/:uid', 'CutDown/save');
// 我的砍价列表
Route::get('/mycut/:limit', 'index/myself/cutdown');

Route::get('/createCut', 'index/Myself/createCut');


Route::get('/order/state/:status', 'index/Order/status');
//确认订单
Route::get('/order/quer', 'index/Order/quer');

/*购物车*/
Route::post('/addcart', 'index/Cart/addToCart');
Route::post('/cart/number', 'index/Cart/number');
Route::get('/cart/pay', 'index/Cart/pay');
Route::get('/cart/:limit', 'index/Cart/index');
Route::delete('/cart/delete/:pid', 'index/Cart/delete');
Route::get('/order/un_pay', 'index/Order/un_pay');
    /*订单*/
Route::get('/order/state/:status', 'index/Order/status');
//未支付
Route::get('/unpaid', 'index/Order/unpaid');
//物流查询
Route::get('/express/:oid', 'index/Order/express');
Route::get('/orderpay/:order', 'index/Order/pay');
//清空缓存
Route::get('/myself/clear', 'index/Myself/clear');
