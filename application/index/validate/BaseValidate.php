<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/14
 * Time: 15:09
 */
namespace app\index\validate;

use app\lib\exception\ParameterException;
use think\Request;
use think\Validate;

class BaseValidate extends Validate
{
    public function goCheck()
    {
        $params = Request::instance()->param();
        $request = $this->batch()->check($params);
        if(!$request) {
            $error = new ParameterException([
                'msg' => $this->error,
            ]);
            throw $error;
        }
        return $params;
    }

    // 判断是否是正整数
    protected function isPositiveInt($value, $rule='')
    {
        if(is_numeric($value) && is_int($value + 0) > 0) {
            return true;
        }
        return false;
    }

    // 是否为空
    protected function isNotEmpty($value, $rule='')
    {
        if(empty($value)){
            return false;
        }
        return true;
    }

    protected function zeroOrOne($value, $rule='')
    {
        if($value == 0 || $value == 1){
            return true;
        }
        return false;
    }

    // 手机号
    protected function isMobile($value)
    {
        $rule = '^1(3|4|5|7|8)[0-9]\d{8}$^';
        $result = preg_match($rule, $value);
        if($result){
            return true;
        }
        return false;
    }
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/14
 * Time: 15:09
 */
namespace app\index\validate;

use app\lib\exception\ParameterException;
use think\Request;
use think\Validate;

class BaseValidate extends Validate
{
    public function goCheck()
    {
        $params = Request::instance()->param();
        $request = $this->batch()->check($params);
        if(!$request) {
            $error = new ParameterException([
                'msg' => $this->error,
            ]);
            throw $error;
        }
        return $params;
    }

    // 判断是否是正整数
    protected function isPositiveInt($value, $rule='')
    {
        if(is_numeric($value) && is_int($value + 0) > 0) {
            return true;
        }
        return false;
    }

    // 是否为空
    protected function isNotEmpty($value, $rule='')
    {
        if(empty($value)){
            return false;
        }
        return true;
    }

    protected function zeroOrOne($value, $rule='')
    {
        if($value == 0 || $value == 1){
            return true;
        }
        return false;
    }

    // 手机号
    protected function isMobile($value)
    {
        $rule = '^1(3|4|5|7|8)[0-9]\d{8}$^';
        $result = preg_match($rule, $value);
        if($result){
            return true;
        }
        return false;
    }
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}