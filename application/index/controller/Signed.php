<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/10/15
 * Time: 23:46
 */

namespace app\index\controller;

use app\model\SignedModel;
use app\model\UserModel;

class Signed extends Auth
{

	static private $grade = [
		'士兵' => 100,
		'排长' => 200,
		'连长' => 500,
		'营长' => 1000,
		'团战' => 2000,
	];

	public function _initialize()
	{
		if (session('Auth.Uid')) {
			$this->uid = session('Auth.Uid');
		} else {
			$this->redirect('/login');
		}
	}

	/**
	 * @var 开放签到接口
	 */
	public function index()
	{
		$info = (new UserModel())->getValue("id = {$this->uid}", "signed");
		if (!$info || date('Y-m-d', $info) != date('Y-m-d', time())) {
			$result = $this->_data($info);
			if ($result){
				$result = [
				'code' => 200,
				'info' => '签到成功!'
				];
			}
		} else {
			$result = [
				'code' => 202,
				'info' => '你今天已经签到了!'
			];
		}
		return json_encode($result);
	}

	/**
	 * @var 检查签到天数
	 */
	private function _read()
	{
		$checkTime = strtotime(date('Y-m-d', strtotime("-6 day")));
		$list = (new UserModel('sy_signed', 'signed_time desc'))->selectAll("uid = {$this->uid} and signed_time >= {$checkTime}", 'signed_time');
		$result = $this->_check($list);
		if ($result) {
			$number = $this->_create(count($list));
		} else {
			$number = 1;
		}
		return $number;
	}

	/**
	 * @var 签到
	 */
	private function _save($id, $arr)
	{
		$result = (new UserModel('', '', '', 'sy_signed', true))->signed($arr, $id);
		if ($result) {
			$info = [
				'code' => 200,
				'info' => '签到成功!'
			];
		} else {
			$info = [
				'code' => 202,
				'info' => '签到失败, 请稍后重试!'
			];
		}
		return $info;
	}

	/**
	 * @var 检查连续签到天数
	 */
	static private function _check($arr)
	{
		$result = false;
		for ($i = 0; $i < count($arr) - 1; $i++) {
			if ($arr[$i]['signed_time'] - $arr[$i + 1]['signed_time'] > 48 * 3600) {
				break;
			}
			$result = true;
		}
		return $result;
	}

	/**
	 * @var 根据签到天数获得对应分值
	 */
	private function _create($num)
	{
		switch ($num) {
			case $num = 2 :
				$number = 2;
				break;
			case $num >= 3 :
				$number = 3;
				break;
			case $num >= 5 :
				$number = 4;
				break;
			case $num > 6 :
				$number = 5;
				break;
		}
		return $number;
	}

	/**
	 * @var 积分页面
	 */
	public function read($limit)
	{
		$list = (new SignedModel('', 'id desc', (int)$limit))->selectAll("uid  = {$this->uid}", "number, status, signed_time");
		if ((int)$limit) {
			return json_encode($list);
		}
		$user = (new UserModel())->findOne("id = {$this->uid}", "intergral, experience, username,signed");
		$state = date('Y-m-d', $user['signed']) != date('Y-m-d', time()) ? false : true;
		$this->assign("state", $state);
		$this->assign("user", $user);
		$this->assign("list", $list);
		return $this->fetch();
	}

	/**
	 * @var 存储调整数据接口
	 */
	public function _data($info, $number = '', $status = '')
	{
		$arr['status'] = 1;
		if ($info && abs(strtotime(date('y-m-d', strtotime("-1 day"))) - $info) < 48 * 3600 - 1) {
			$arr['number'] = self::_read();
		} elseif ($number && $status) {
			$arr['number'] = $number;
			$arr['status'] = $status;
		} else {
			$arr['number'] = 1;
		}
		$arr['uid'] = $this->uid;
		$arr['signed_time'] = time();
		$arr['sign_time'] = date('Y-m-d', time());
		$arr['signed_time'] = time();
		$arr['sign_time'] = date('Y-m-d', time());
		$result = $this->_save($this->uid,$arr);
		return $result;
	}

	public function grade()
	{
		return json_encode(self::$grade);
	}

	public function create($uid, $number, $status)
	{
		$arr['number'] = $number;
		$arr['status'] = $status;
		$arr['uid'] = $uid;
		$arr['signed_time'] = time();
		$arr['sign_time'] = date('Y-m-d', time());
		$arr['signed_time'] = time();
		$arr['sign_time'] = date('Y-m-d', time());
		$result = $this->_save($uid, $arr);
		return $result;
	}
}