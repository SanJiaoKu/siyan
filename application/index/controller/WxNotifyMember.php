<<<<<<< HEAD
<?php

namespace app\index\controller;


use app\index\model\Agent;
use app\index\model\AgentDaybook;
use app\index\model\Card;
use app\index\model\CardDaybook;
use app\index\model\Message;
use app\index\model\RechargeMember;
use app\index\model\User;
use app\index\model\UserDaybook;
use app\lib\exception\DatabaseException;
use think\Db;
use think\Exception;
use think\Loader;

Loader::import('WxPay.WxPay', EXTEND_PATH, '.Api.php');

class WxNotifyMember extends \WxPayNotify
{
    //<xml> WxNotifyMember
    //<appid><![CDATA[wx2421b1c4370ec43b]]></appid>
    //<attach><![CDATA[支付测试]]></attach>
    //<bank_type><![CDATA[CFT]]></bank_type>
    //<fee_type><![CDATA[CNY]]></fee_type>
    //<is_subscribe><![CDATA[Y]]></is_subscribe>
    //<mch_id><![CDATA[10000100]]></mch_id>
    //<nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>
    //<openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>
    //<out_trade_no><![CDATA[1409811653]]></out_trade_no>
    //<result_code><![CDATA[SUCCESS]]></result_code>
    //<return_code><![CDATA[SUCCESS]]></return_code>
    //<sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>
    //<sub_mch_id><![CDATA[10000100]]></sub_mch_id>
    //<time_end><![CDATA[20140903131540]]></time_end>
    //<total_fee>1</total_fee> 订单支付金额
    //<trade_type><![CDATA[JSAPI]]></trade_type>
    //<transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>
    //</xml>

    public function NotifyProcess($data, &$msg)
    {
        if ($data['result_code'] == 'SUCCESS') {
            // 刚刚传入的订单号
            $orderNo = $data['out_trade_no'];
            $pay_price = $data['total_fee'];
            // 刚刚传入的订单号
            $member_pay = RechargeMember::where('billno', '=', $orderNo)
                ->where('is_pay', '=', 0)
                ->find();
            if(!$member_pay) {
                return true;
            } else {
                try {
                    // 支付成功 修改状态
                    Db::startTrans();
                    $member_pay->is_pay = 1;
                    $member_pay->save();
                    // 将此用户的 is_vip 修改为 1
                    $result = User::where('id', $member_pay->uid)
                        ->update(['is_vip' => 1]);
                    if(!$result) {
                        Db::rollback();
                        return true;
                    }
                    $wx_card = Card::where('id', '=', 2)
                        ->find();
                    // 写银行卡的流水
                    $daybook = [
                        'before_balance' => $wx_card->balance,
                        'billno' => $orderNo,
                        'admin_uid' => 9,
                        'income' => $pay_price/100,
                        'description' => 'id为' . $member_pay->uid . '充值成为会员' . $pay_price,
                        'fee' => 0,
                        'card_id' => 2,
                        'type' => 2,
                        'uid' => $member_pay->uid,
                        'date' => date('Y-m-d')
                    ];
                    $wx_card->balance = ($wx_card->balance * 100 + $pay_price)/100;
                    $wx_card->save();
                    $daybook['after_balance'] = $wx_card->balance;
                    CardDaybook::insert($daybook);
                    $result = $this->member_allocation($orderNo);
                    if($result) {
                        Db::commit();
                        return true;
                    }
                    Db::rollback();
                    return true;
                } catch (Exception $e) {
                    Db::rollback();
                    $member_pay->is_pay = 9;
                    $member_pay->save();
                    return true;
                }
            }
        }else {
            return true;
        }
    }

    public function member_allocation($billno)
    {
        // 拿到了用户的订单
        try {
            // 根据 id 号码获得 用户信息(包含上级代理和上上级代理的邀请码)
            $order_info = $this->getInfo($billno);
            if(!$order_info) {
                return false;
            }
            $order_no = $order_info->billno;
            // 获取总代理信息
            $agent_info = $this->agent($order_info->user->agent_id);
            if(!$agent_info) {
                return false;
            }
//            $billno = billno(); // 分成订单号
            $user_info = $order_info->user; // 用户信息(买家)
            $type = 'income'; // 分成定义为收入
            $price = '99'; // 分成金额
            $agent_to_daybook = [
                'agent_id' => $agent_info->id,
                'before_balance' => $agent_info->agent_balance,
            ];
            // TODO 有上级代理(NO) 上上级代理(NO) 总代理
            if($user_info->p_id == 0) {
                // 没有上级代理和上上级代理 只写总代理流水
                // 准备数据 分成价格 和分成比例
                $ratio = 1; // 总代理全部获得
                // 写总代理余额
                $agent_balance_result = $this->agent_balance($agent_info, $price, $ratio);
                if(!$agent_balance_result) {
                    Db::rollback();
                    return false;
                }
                $agent_to_daybook['after_balance'] = $agent_info->agent_balance;
                // 写总代理流水
                $agent_daybook = $this->agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type);
                if(!$agent_daybook) {
                    Db::rollback();
                    return false;
                }
            }
            // TODO 有上级代理 上上级代理(NO) 总代理(OK)
            if($user_info->p_id !== 0 && $user_info->g_id == 0) {
                $ratio = 0.6;
                // 获得上一级代理信息
                $parent_info = $this->getUserInfo($user_info->p_id);
                if(!$parent_info) {
                    Db::rollback();
                    return false;
                }
                $parent_info_balance['before_balance'] = $parent_info->balance;
                // 增加上一级代理余额(这里分6成)
                $parent_balance = $this->changeUserBalance($parent_info, $price, $ratio);
                if(!$parent_balance) {
                    Db::rollback();
                    return false;
                }
                $parent_info_balance['after_balance'] = $parent_info->balance;
                // 写上一级代理流水(6成)
                $parent_daybook = $this->userDaybook($order_no, $parent_info, $price, $ratio, $billno, $parent_info_balance, $type);
                if(!$parent_daybook) {
                    Db::rollback();
                    return false;
                }
                // 给上一级代理发送消息
                $comment = '恭喜您获得了您的下线呢称为【' . $user_info->username . '】的会员分红【' . $price *  $ratio . '】元, 四眼文化祝您天天开心';
                $this->sendMsg($parent_info->id, '获得下线代理分红', $comment);
                // 写总代理余额(4成)
                $ratio = 0.4;
                $agent_balance_result = $this->agent_balance($agent_info, $price, $ratio);
                if(!$agent_balance_result) {
                    Db::rollback();
                    return false;
                }
                // 写总代理流水
                $agent_to_daybook['after_balance'] = $agent_info->agent_balance;
                $agent_daybook = $this->agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type);
                if(!$agent_daybook) {
                    Db::rollback();
                    return false;
                }
            }
            // TODO 有上级代理 上上级代理 总代理
            if($user_info->p_id !== 0 && $user_info->g_id !== 0) {
                // 获得上一级代理信息
                $parent_info = $this->getUserInfo($user_info->p_id);
                $parent_info_balance['before_balance'] = $parent_info->balance;
                if(!$parent_info) {
                    Db::rollback();
                    return false;
                }
                $ratio = 0.6;
                // 写上一级代理余额(6成)
                $parent_balance = $this->changeUserBalance($parent_info, $price, $ratio);
                $parent_info_balance['after_balance'] = $parent_info->balance;
                if(!$parent_balance) {
                    Db::rollback();
                    return false;
                }
                // 写上一级代理流水(6成)
                $parent_daybook = $this->userDaybook($order_no, $parent_info, $price, $ratio, $billno, $parent_info_balance, $type);
                if(!$parent_daybook) {
                    Db::rollback();
                    return false;
                }
                // 给上一级代理发信息(6成)
                $comment = '恭喜您获得了您的下线呢称为【' . $user_info->username . '】的会员分红【' . $price * $ratio . '】元, 四眼文化祝您天天开心';
                $this->sendMsg($parent_info->id, '获得下线代理分红', $comment);
                // 获得上2级代理信息
                $grand_info = $this->getUserInfo($user_info->g_id);
                $grand_info_balance['before_balance'] = $grand_info->balance;
                if(!$grand_info) {
                    Db::rollback();
                    return false;
                }
                // 写上两级代理余额(6成)(3成)
                $ratio = 0.3;
                $grand_balance = $this->changeUserBalance($grand_info, $price, $ratio);
                $grand_info_balance['after_balance'] = $grand_info->balance;
                if(!$grand_balance) {
                    Db::rollback();
                    return false;
                }
                // 写上2级代理流水(3成)
                $grand_daybook = $this->userDaybook($order_no, $grand_info, $price, $ratio, $billno, $grand_info_balance, $type);
                if(!$grand_daybook) {
                    Db::rollback();
                    return false;
                }
                // 给上一2级代理发信息(3成)
                $comment = '恭喜您获得了您的下线[' . $parent_info->username . ']发展的下线的呢称为【' . $user_info->username . '】的订单分红【' . $price * $ratio . '】元, 四眼文化祝您天天开心';
                $this->sendMsg($grand_info->id, '获得下线发展下线代理分红', $comment);
                // 写总代理余额(1成)
                $ratio = 0.1;
                $agent_balance_result = $this->agent_balance($agent_info, $price, $ratio);
                if(!$agent_balance_result) {
                    Db::rollback();
                    return false;
                }
                $agent_to_daybook['after_balance'] = $agent_info->agent_balance;
                // 写总代理流水(1成)
                $agent_daybook = $this->agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type);
                if(!$agent_daybook) {
                    Db::rollback();
                    return false;
                }
            }
            // 把状态订单改为 已分成状态
            $order_info->allocation = 1;
            $result = $order_info->save();
            if($result) {
                return true;
            }
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
    }

    // 获得订单信息 用户信息
    public function getInfo($billno)
    {
        try {
            $info = RechargeMember::with(['user'])
                ->where('billno', $billno)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $info;
    }

    // 总代理信息
    public function agent($id)
    {
        try {
            $info = Agent::where('id', $id)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $info;
    }

    /**
     * @param $order_no 订单号(购买订单订单号)
     * @param $user_info 用户的 详细信息
     * @param $price 分成价格
     * @param $ratio 分成比例
     * @param $billno 订单号(新生成的分成订单号而不是买东西的订单号)
     * @param $agent_to_daybook 需要的流水信息
     * @param $type income outlay
     * @return int|string
     * @throws DatabaseException
     */
    public function agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type)
    {
        try {
            // 进帐/出账价格
            $price = $price * 1000 * $ratio;
            $data = [
                'uid' => $user_info->id,
                'admin_uid' => 999,
                'billno' => $billno,
                'agent_id' => $agent_to_daybook['agent_id'],
                'before_balance' => $agent_to_daybook['before_balance'],
                'after_balance' => $agent_to_daybook['after_balance'],
                'description' => '用户名 : ' . $user_info->username . '的用户购买的订单号为' . $order_no . '为您分得比例为【' . $ratio . '】代理金【' . $price/1000 . '】元'
            ];
            if($type == 'income') {
                $data['income'] = $price/1000;
            }
            if($type == 'outlay') {
                $data['outlay'] = $price/1000;
            }
            $result = AgentDaybook::insert($data);
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    // 写总代理余额
    public function agent_balance($agent_info, $price, $ratio)
    {
        $before_balance = $agent_info->agent_balance;
        // 代理获得收入
        $after_balance = ($before_balance * 1000 + $price * 1000 * $ratio)/1000;
        $agent_info->agent_balance = $after_balance;
        $result = $agent_info->save();
        return $result;
    }

    public function getUserInfo($id)
    {
        try {
            $user_info = User::where('id', $id)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $user_info;
    }

    // 改变用户余额
    public function changeUserBalance($user_info, $price, $ratio)
    {
        $before_balance = $user_info->balance;
        // 代理获得收入
        $after_balance = ($before_balance * 1000 + $price * 1000 * $ratio)/1000;
        $user_info->balance = $after_balance;
        $result = $user_info->save();
        return $result;
    }

    // 增加用户余额流水
    public function userDaybook($order_no, $user_info, $price, $ratio, $billno, $parent_info_balance, $type)
    {
        try {
            // 进帐/出账价格
            $price = $price * 1000 * $ratio;
            $data = [
                'uid' => $user_info->id,
                'type' => 1, // 转入
                'admin_uid' => 999,
                'billno' => $billno,
                'before_balance' => $parent_info_balance['before_balance'],
                'after_balance' => $parent_info_balance['after_balance'],
                'description' => '用户名 : ' . $user_info->username . '的用户购买的订单号为' . $order_no . '为您分得代理金【' . $price/1000 . '】元, 比例为' . $ratio
            ];
            if($type == 'income') {
                $data['income'] = $price/1000;
            }
            if($type == 'outlay') {
                $data['outlay'] = $price/1000;
            }
            $result = UserDaybook::insert($data);
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    public function sendMsg($id, $title, $comment)
    {
        $prep_data = [
            'uid' => $id,
            'admin_uid' => 999,
            'msg_title' => $title,
            'msg_contents' => $comment,
            'msg_from' => 999,
            'status' => 1
        ];
        try {
            $result = Message::insert($prep_data);
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    public function log($content)
    {
        Db::table('log_test')->insert($content);
    }
=======
<?php

namespace app\index\controller;


use app\index\model\Agent;
use app\index\model\AgentDaybook;
use app\index\model\Card;
use app\index\model\CardDaybook;
use app\index\model\Message;
use app\index\model\RechargeMember;
use app\index\model\User;
use app\index\model\UserDaybook;
use app\lib\exception\DatabaseException;
use think\Db;
use think\Exception;
use think\Loader;

Loader::import('WxPay.WxPay', EXTEND_PATH, '.Api.php');

class WxNotifyMember extends \WxPayNotify
{
    //<xml> WxNotifyMember
    //<appid><![CDATA[wx2421b1c4370ec43b]]></appid>
    //<attach><![CDATA[支付测试]]></attach>
    //<bank_type><![CDATA[CFT]]></bank_type>
    //<fee_type><![CDATA[CNY]]></fee_type>
    //<is_subscribe><![CDATA[Y]]></is_subscribe>
    //<mch_id><![CDATA[10000100]]></mch_id>
    //<nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>
    //<openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>
    //<out_trade_no><![CDATA[1409811653]]></out_trade_no>
    //<result_code><![CDATA[SUCCESS]]></result_code>
    //<return_code><![CDATA[SUCCESS]]></return_code>
    //<sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>
    //<sub_mch_id><![CDATA[10000100]]></sub_mch_id>
    //<time_end><![CDATA[20140903131540]]></time_end>
    //<total_fee>1</total_fee> 订单支付金额
    //<trade_type><![CDATA[JSAPI]]></trade_type>
    //<transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>
    //</xml>

    public function NotifyProcess($data, &$msg)
    {
        if ($data['result_code'] == 'SUCCESS') {
            // 刚刚传入的订单号
            $orderNo = $data['out_trade_no'];
            $pay_price = $data['total_fee'];
            // 刚刚传入的订单号
            $member_pay = RechargeMember::where('billno', '=', $orderNo)
                ->where('is_pay', '=', 0)
                ->find();
            if(!$member_pay) {
                return true;
            } else {
                try {
                    // 支付成功 修改状态
                    Db::startTrans();
                    $member_pay->is_pay = 1;
                    $member_pay->save();
                    // 将此用户的 is_vip 修改为 1
                    $result = User::where('id', $member_pay->uid)
                        ->update(['is_vip' => 1]);
                    if(!$result) {
                        Db::rollback();
                        return true;
                    }
                    $wx_card = Card::where('id', '=', 2)
                        ->find();
                    // 写银行卡的流水
                    $daybook = [
                        'before_balance' => $wx_card->balance,
                        'billno' => $orderNo,
                        'admin_uid' => 9,
                        'income' => $pay_price/100,
                        'description' => 'id为' . $member_pay->uid . '充值成为会员' . $pay_price,
                        'fee' => 0,
                        'card_id' => 2,
                        'type' => 2,
                        'uid' => $member_pay->uid,
                        'date' => date('Y-m-d')
                    ];
                    $wx_card->balance = ($wx_card->balance * 100 + $pay_price)/100;
                    $wx_card->save();
                    $daybook['after_balance'] = $wx_card->balance;
                    CardDaybook::insert($daybook);
                    $result = $this->member_allocation($orderNo);
                    if($result) {
                        Db::commit();
                        return true;
                    }
                    Db::rollback();
                    return true;
                } catch (Exception $e) {
                    Db::rollback();
                    $member_pay->is_pay = 9;
                    $member_pay->save();
                    return true;
                }
            }
        }else {
            return true;
        }
    }

    public function member_allocation($billno)
    {
        // 拿到了用户的订单
        try {
            // 根据 id 号码获得 用户信息(包含上级代理和上上级代理的邀请码)
            $order_info = $this->getInfo($billno);
            if(!$order_info) {
                return false;
            }
            $order_no = $order_info->billno;
            // 获取总代理信息
            $agent_info = $this->agent($order_info->user->agent_id);
            if(!$agent_info) {
                return false;
            }
//            $billno = billno(); // 分成订单号
            $user_info = $order_info->user; // 用户信息(买家)
            $type = 'income'; // 分成定义为收入
            $price = '99'; // 分成金额
            $agent_to_daybook = [
                'agent_id' => $agent_info->id,
                'before_balance' => $agent_info->agent_balance,
            ];
            // TODO 有上级代理(NO) 上上级代理(NO) 总代理
            if($user_info->p_id == 0) {
                // 没有上级代理和上上级代理 只写总代理流水
                // 准备数据 分成价格 和分成比例
                $ratio = 1; // 总代理全部获得
                // 写总代理余额
                $agent_balance_result = $this->agent_balance($agent_info, $price, $ratio);
                if(!$agent_balance_result) {
                    Db::rollback();
                    return false;
                }
                $agent_to_daybook['after_balance'] = $agent_info->agent_balance;
                // 写总代理流水
                $agent_daybook = $this->agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type);
                if(!$agent_daybook) {
                    Db::rollback();
                    return false;
                }
            }
            // TODO 有上级代理 上上级代理(NO) 总代理(OK)
            if($user_info->p_id !== 0 && $user_info->g_id == 0) {
                $ratio = 0.6;
                // 获得上一级代理信息
                $parent_info = $this->getUserInfo($user_info->p_id);
                if(!$parent_info) {
                    Db::rollback();
                    return false;
                }
                $parent_info_balance['before_balance'] = $parent_info->balance;
                // 增加上一级代理余额(这里分6成)
                $parent_balance = $this->changeUserBalance($parent_info, $price, $ratio);
                if(!$parent_balance) {
                    Db::rollback();
                    return false;
                }
                $parent_info_balance['after_balance'] = $parent_info->balance;
                // 写上一级代理流水(6成)
                $parent_daybook = $this->userDaybook($order_no, $parent_info, $price, $ratio, $billno, $parent_info_balance, $type);
                if(!$parent_daybook) {
                    Db::rollback();
                    return false;
                }
                // 给上一级代理发送消息
                $comment = '恭喜您获得了您的下线呢称为【' . $user_info->username . '】的会员分红【' . $price *  $ratio . '】元, 四眼文化祝您天天开心';
                $this->sendMsg($parent_info->id, '获得下线代理分红', $comment);
                // 写总代理余额(4成)
                $ratio = 0.4;
                $agent_balance_result = $this->agent_balance($agent_info, $price, $ratio);
                if(!$agent_balance_result) {
                    Db::rollback();
                    return false;
                }
                // 写总代理流水
                $agent_to_daybook['after_balance'] = $agent_info->agent_balance;
                $agent_daybook = $this->agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type);
                if(!$agent_daybook) {
                    Db::rollback();
                    return false;
                }
            }
            // TODO 有上级代理 上上级代理 总代理
            if($user_info->p_id !== 0 && $user_info->g_id !== 0) {
                // 获得上一级代理信息
                $parent_info = $this->getUserInfo($user_info->p_id);
                $parent_info_balance['before_balance'] = $parent_info->balance;
                if(!$parent_info) {
                    Db::rollback();
                    return false;
                }
                $ratio = 0.6;
                // 写上一级代理余额(6成)
                $parent_balance = $this->changeUserBalance($parent_info, $price, $ratio);
                $parent_info_balance['after_balance'] = $parent_info->balance;
                if(!$parent_balance) {
                    Db::rollback();
                    return false;
                }
                // 写上一级代理流水(6成)
                $parent_daybook = $this->userDaybook($order_no, $parent_info, $price, $ratio, $billno, $parent_info_balance, $type);
                if(!$parent_daybook) {
                    Db::rollback();
                    return false;
                }
                // 给上一级代理发信息(6成)
                $comment = '恭喜您获得了您的下线呢称为【' . $user_info->username . '】的会员分红【' . $price * $ratio . '】元, 四眼文化祝您天天开心';
                $this->sendMsg($parent_info->id, '获得下线代理分红', $comment);
                // 获得上2级代理信息
                $grand_info = $this->getUserInfo($user_info->g_id);
                $grand_info_balance['before_balance'] = $grand_info->balance;
                if(!$grand_info) {
                    Db::rollback();
                    return false;
                }
                // 写上两级代理余额(6成)(3成)
                $ratio = 0.3;
                $grand_balance = $this->changeUserBalance($grand_info, $price, $ratio);
                $grand_info_balance['after_balance'] = $grand_info->balance;
                if(!$grand_balance) {
                    Db::rollback();
                    return false;
                }
                // 写上2级代理流水(3成)
                $grand_daybook = $this->userDaybook($order_no, $grand_info, $price, $ratio, $billno, $grand_info_balance, $type);
                if(!$grand_daybook) {
                    Db::rollback();
                    return false;
                }
                // 给上一2级代理发信息(3成)
                $comment = '恭喜您获得了您的下线[' . $parent_info->username . ']发展的下线的呢称为【' . $user_info->username . '】的订单分红【' . $price * $ratio . '】元, 四眼文化祝您天天开心';
                $this->sendMsg($grand_info->id, '获得下线发展下线代理分红', $comment);
                // 写总代理余额(1成)
                $ratio = 0.1;
                $agent_balance_result = $this->agent_balance($agent_info, $price, $ratio);
                if(!$agent_balance_result) {
                    Db::rollback();
                    return false;
                }
                $agent_to_daybook['after_balance'] = $agent_info->agent_balance;
                // 写总代理流水(1成)
                $agent_daybook = $this->agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type);
                if(!$agent_daybook) {
                    Db::rollback();
                    return false;
                }
            }
            // 把状态订单改为 已分成状态
            $order_info->allocation = 1;
            $result = $order_info->save();
            if($result) {
                return true;
            }
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
    }

    // 获得订单信息 用户信息
    public function getInfo($billno)
    {
        try {
            $info = RechargeMember::with(['user'])
                ->where('billno', $billno)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $info;
    }

    // 总代理信息
    public function agent($id)
    {
        try {
            $info = Agent::where('id', $id)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $info;
    }

    /**
     * @param $order_no 订单号(购买订单订单号)
     * @param $user_info 用户的 详细信息
     * @param $price 分成价格
     * @param $ratio 分成比例
     * @param $billno 订单号(新生成的分成订单号而不是买东西的订单号)
     * @param $agent_to_daybook 需要的流水信息
     * @param $type income outlay
     * @return int|string
     * @throws DatabaseException
     */
    public function agent_daybook($order_no, $user_info, $price, $ratio, $billno, $agent_to_daybook, $type)
    {
        try {
            // 进帐/出账价格
            $price = $price * 1000 * $ratio;
            $data = [
                'uid' => $user_info->id,
                'admin_uid' => 999,
                'billno' => $billno,
                'agent_id' => $agent_to_daybook['agent_id'],
                'before_balance' => $agent_to_daybook['before_balance'],
                'after_balance' => $agent_to_daybook['after_balance'],
                'description' => '用户名 : ' . $user_info->username . '的用户购买的订单号为' . $order_no . '为您分得比例为【' . $ratio . '】代理金【' . $price/1000 . '】元'
            ];
            if($type == 'income') {
                $data['income'] = $price/1000;
            }
            if($type == 'outlay') {
                $data['outlay'] = $price/1000;
            }
            $result = AgentDaybook::insert($data);
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    // 写总代理余额
    public function agent_balance($agent_info, $price, $ratio)
    {
        $before_balance = $agent_info->agent_balance;
        // 代理获得收入
        $after_balance = ($before_balance * 1000 + $price * 1000 * $ratio)/1000;
        $agent_info->agent_balance = $after_balance;
        $result = $agent_info->save();
        return $result;
    }

    public function getUserInfo($id)
    {
        try {
            $user_info = User::where('id', $id)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $user_info;
    }

    // 改变用户余额
    public function changeUserBalance($user_info, $price, $ratio)
    {
        $before_balance = $user_info->balance;
        // 代理获得收入
        $after_balance = ($before_balance * 1000 + $price * 1000 * $ratio)/1000;
        $user_info->balance = $after_balance;
        $result = $user_info->save();
        return $result;
    }

    // 增加用户余额流水
    public function userDaybook($order_no, $user_info, $price, $ratio, $billno, $parent_info_balance, $type)
    {
        try {
            // 进帐/出账价格
            $price = $price * 1000 * $ratio;
            $data = [
                'uid' => $user_info->id,
                'type' => 1, // 转入
                'admin_uid' => 999,
                'billno' => $billno,
                'before_balance' => $parent_info_balance['before_balance'],
                'after_balance' => $parent_info_balance['after_balance'],
                'description' => '用户名 : ' . $user_info->username . '的用户购买的订单号为' . $order_no . '为您分得代理金【' . $price/1000 . '】元, 比例为' . $ratio
            ];
            if($type == 'income') {
                $data['income'] = $price/1000;
            }
            if($type == 'outlay') {
                $data['outlay'] = $price/1000;
            }
            $result = UserDaybook::insert($data);
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    public function sendMsg($id, $title, $comment)
    {
        $prep_data = [
            'uid' => $id,
            'admin_uid' => 999,
            'msg_title' => $title,
            'msg_contents' => $comment,
            'msg_from' => 999,
            'status' => 1
        ];
        try {
            $result = Message::insert($prep_data);
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    public function log($content)
    {
        Db::table('log_test')->insert($content);
    }
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}