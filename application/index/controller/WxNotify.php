<<<<<<< HEAD
<?php

namespace app\index\controller;


use app\index\model\BySale;
use app\index\model\Card;
use app\index\model\CardDaybook;
use app\index\model\Order;
use app\index\model\User;
use think\cache\driver\Redis;
use think\Db;
use think\Loader;
use app\model\UserModel;
use app\model\OrderModel;

Loader::import('WxPay.WxPay', EXTEND_PATH, '.Api.php');

class WxNotify extends \WxPayNotify
{
//    //<xml>
//    //<appid><![CDATA[wx2421b1c4370ec43b]]></appid>
//    //<attach><![CDATA[支付测试]]></attach>
//    //<bank_type><![CDATA[CFT]]></bank_type>
//    //<fee_type><![CDATA[CNY]]></fee_type>
//    //<is_subscribe><![CDATA[Y]]></is_subscribe>
//    //<mch_id><![CDATA[10000100]]></mch_id>
//    //<nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>
//    //<openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>
//    //<out_trade_no><![CDATA[1409811653]]></out_trade_no>
//    //<result_code><![CDATA[SUCCESS]]></result_code>
//    //<return_code><![CDATA[SUCCESS]]></return_code>
//    //<sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>
//    //<sub_mch_id><![CDATA[10000100]]></sub_mch_id>
//    //<time_end><![CDATA[20140903131540]]></time_end>
//    //<total_fee>1</total_fee> 订单支付金额
//    //<trade_type><![CDATA[JSAPI]]></trade_type>
//    //<transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>
//    //</xml>
//
//    public function NotifyProcess($data, &$msg)
//    {
//
//        if ($data['result_code'] == 'SUCCESS') {
//            // 刚刚传入的订单号
//            $orderNo = $data['out_trade_no'];
//            // 刚刚传入的订单号
//            $product_info = Order::with(['product'])
//                ->field('id,order_no,user_id')
//                ->where('status', '=', 1)
//                ->where('order_no', '=', $orderNo)
//                ->find();
//            if(!$product_info) {
//                return true;
//            } else {
//                // 支付成功 需要减库存
//                // 查看商品总类
//                $product_array = $product_info->toArray();
//                $product_all = $product_array['product'];
//                $pay_price = $data['total_fee'];
//                try {
//                    Db::startTrans();
//                    $sql = 'UPDATE sy_product_info SET stock = CASE id ';
//                    $ids = '';
//                    foreach ($product_all as $value) {
//                        $ids .= ',' . $value['product_id'];
//                        $str = " stock - $value[num]";
//                        $sql .= sprintf("WHEN %d THEN %s ", $value['product_id'], $str);
//                    }
//                    $ids = substr($ids, 1);
//                    $sql .= " END WHERE id IN ($ids)";
//                    Db::query($sql);
//                    // 看看是否有优惠 现在只有一个sale sale_id=1
//                    $all_sale = BySale::where('billno', $orderNo)
//                        ->where('is_pay', '=', 0)
//                        ->find();
//                    if($all_sale) {
//                        $all_sale->is_pay = 1;
//                        $all_sale->save();
//                    }
//                    // 给微信支付卡增加余额
//                    $wx_card = Card::where('id', '=', 2)
//                        ->find();
//                    // 写银行卡的流水
//                    $daybook = [
//                        'before_balance' => $wx_card->balance,
//                        'billno' => $orderNo,
//                        'admin_uid' => 9,
//                        'income' => $pay_price,
//                        'description' => 'id为' . $product_info->user_id . '的用户购物花费' . $pay_price,
//                        'fee' => 0,
//                        'card_id' => 2,
//                        'type' => 1,
//                        'uid' => $product_info->user_id,
//                        'date' => date('Y-m-d')
//                    ];
//                    $wx_card->balance = ($wx_card->balance * 100 + $pay_price)/100;
//                    $wx_card->save();
//                    $daybook['after_balance'] = $wx_card->balance;
//                    CardDaybook::insert($daybook);
//                } catch (\Exception $e) {
//                    Db::rollback();
//                    // 支付完成 还是需要修改订单
//                    $product_info->status = 9; // 订单支付成功 减少库存出错
//                    $product_info->snap_name = $e->getMessage(); // 订单支付成功 减少库存出错
//                    $product_info->save();
//                    return true;
//                }
//                $product_info->status = 2;
//                $product_info->save();
//                // 需要给用户增加相应的积分
//                User::where('id', $product_info->user_id)
//                    ->setInc('integral', intval(ceil($pay_price)));
//                Db::commit();
//                return true;
//            }
//        }else {
//            return true;
//        }
//    }
	/**
	 * @var 微信支付回调处理
	 */
	public function NotifyProcess($data, &$msg)
	{
		if ($data['result_code'] == 'SUCCESS') {
			if ((new Redis())->get($data['out_trade_no'])) {
				$type = substr($data['out_trade_no'], 0, 2);
				if ($type == 'vp') {
					$result = self::_vip($data['out_trade_no']);
				} elseif ($type == 'by') {
					$result = self::_buy($data['out_trade_no']);
				}
				return $result;
			}
		}
		return true;
	}

	/**
	 * @var 购买
	 */
	static private function _buy($orderNum)
	{
		$orderInfo = (new Redis())->get($orderNum);
        $result = (new OrderModel())->createOrder($orderInfo['order'], $orderInfo['info'], $orderInfo['coupon'], $orderInfo['express'], $orderNum);
		return $result;
	}


	/**
	 * @var vip
	 */
	static private function _vip($orderNum)
	{
		$orderInfo = (new Redis())->get($orderNum);
		$arr['uid'] = $orderInfo;
		$arr['money'] = 99;
		$arr['billno'] = $orderNum;
		$arr['is_pay'] = 1;
		$result = (new UserModel())->beVip($arr, $orderInfo, $orderNum);
		return $result;
	}
=======
<?php

namespace app\index\controller;


use app\index\model\BySale;
use app\index\model\Card;
use app\index\model\CardDaybook;
use app\index\model\Order;
use app\index\model\User;
use think\cache\driver\Redis;
use think\Db;
use think\Loader;
use app\model\UserModel;
use app\model\OrderModel;

Loader::import('WxPay.WxPay', EXTEND_PATH, '.Api.php');

class WxNotify extends \WxPayNotify
{
//    //<xml>
//    //<appid><![CDATA[wx2421b1c4370ec43b]]></appid>
//    //<attach><![CDATA[支付测试]]></attach>
//    //<bank_type><![CDATA[CFT]]></bank_type>
//    //<fee_type><![CDATA[CNY]]></fee_type>
//    //<is_subscribe><![CDATA[Y]]></is_subscribe>
//    //<mch_id><![CDATA[10000100]]></mch_id>
//    //<nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>
//    //<openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>
//    //<out_trade_no><![CDATA[1409811653]]></out_trade_no>
//    //<result_code><![CDATA[SUCCESS]]></result_code>
//    //<return_code><![CDATA[SUCCESS]]></return_code>
//    //<sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>
//    //<sub_mch_id><![CDATA[10000100]]></sub_mch_id>
//    //<time_end><![CDATA[20140903131540]]></time_end>
//    //<total_fee>1</total_fee> 订单支付金额
//    //<trade_type><![CDATA[JSAPI]]></trade_type>
//    //<transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>
//    //</xml>
//
//    public function NotifyProcess($data, &$msg)
//    {
//
//        if ($data['result_code'] == 'SUCCESS') {
//            // 刚刚传入的订单号
//            $orderNo = $data['out_trade_no'];
//            // 刚刚传入的订单号
//            $product_info = Order::with(['product'])
//                ->field('id,order_no,user_id')
//                ->where('status', '=', 1)
//                ->where('order_no', '=', $orderNo)
//                ->find();
//            if(!$product_info) {
//                return true;
//            } else {
//                // 支付成功 需要减库存
//                // 查看商品总类
//                $product_array = $product_info->toArray();
//                $product_all = $product_array['product'];
//                $pay_price = $data['total_fee'];
//                try {
//                    Db::startTrans();
//                    $sql = 'UPDATE sy_product_info SET stock = CASE id ';
//                    $ids = '';
//                    foreach ($product_all as $value) {
//                        $ids .= ',' . $value['product_id'];
//                        $str = " stock - $value[num]";
//                        $sql .= sprintf("WHEN %d THEN %s ", $value['product_id'], $str);
//                    }
//                    $ids = substr($ids, 1);
//                    $sql .= " END WHERE id IN ($ids)";
//                    Db::query($sql);
//                    // 看看是否有优惠 现在只有一个sale sale_id=1
//                    $all_sale = BySale::where('billno', $orderNo)
//                        ->where('is_pay', '=', 0)
//                        ->find();
//                    if($all_sale) {
//                        $all_sale->is_pay = 1;
//                        $all_sale->save();
//                    }
//                    // 给微信支付卡增加余额
//                    $wx_card = Card::where('id', '=', 2)
//                        ->find();
//                    // 写银行卡的流水
//                    $daybook = [
//                        'before_balance' => $wx_card->balance,
//                        'billno' => $orderNo,
//                        'admin_uid' => 9,
//                        'income' => $pay_price,
//                        'description' => 'id为' . $product_info->user_id . '的用户购物花费' . $pay_price,
//                        'fee' => 0,
//                        'card_id' => 2,
//                        'type' => 1,
//                        'uid' => $product_info->user_id,
//                        'date' => date('Y-m-d')
//                    ];
//                    $wx_card->balance = ($wx_card->balance * 100 + $pay_price)/100;
//                    $wx_card->save();
//                    $daybook['after_balance'] = $wx_card->balance;
//                    CardDaybook::insert($daybook);
//                } catch (\Exception $e) {
//                    Db::rollback();
//                    // 支付完成 还是需要修改订单
//                    $product_info->status = 9; // 订单支付成功 减少库存出错
//                    $product_info->snap_name = $e->getMessage(); // 订单支付成功 减少库存出错
//                    $product_info->save();
//                    return true;
//                }
//                $product_info->status = 2;
//                $product_info->save();
//                // 需要给用户增加相应的积分
//                User::where('id', $product_info->user_id)
//                    ->setInc('integral', intval(ceil($pay_price)));
//                Db::commit();
//                return true;
//            }
//        }else {
//            return true;
//        }
//    }
	/**
	 * @var 微信支付回调处理
	 */
	public function NotifyProcess($data, &$msg)
	{
		if ($data['result_code'] == 'SUCCESS') {
			if ((new Redis())->get($data['out_trade_no'])) {
				$type = substr($data['out_trade_no'], 0, 2);
				if ($type == 'vp') {
					$result = self::_vip($data['out_trade_no']);
				} elseif ($type == 'by') {
					$result = self::_buy($data['out_trade_no']);
				}
				return $result;
			}
		}
		return true;
	}

	/**
	 * @var 购买
	 */
	static private function _buy($orderNum)
	{
		$orderInfo = (new Redis())->get($orderNum);
        $result = (new OrderModel())->createOrder($orderInfo['order'], $orderInfo['info'], $orderInfo['coupon'], $orderInfo['express'], $orderNum);
		return $result;
	}


	/**
	 * @var vip
	 */
	static private function _vip($orderNum)
	{
		$orderInfo = (new Redis())->get($orderNum);
		$arr['uid'] = $orderInfo;
		$arr['money'] = 99;
		$arr['billno'] = $orderNum;
		$arr['is_pay'] = 1;
		$result = (new UserModel())->beVip($arr, $orderInfo, $orderNum);
		return $result;
	}
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}