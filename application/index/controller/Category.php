<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/5
 * Time: 10:58
 */

namespace app\index\controller;

use app\index\server\CategoryServer;
use app\index\validate\IDMustBePostiveInt;
use think\Controller;

class Category extends Controller
{
    public function index()
    {
        $category = new CategoryServer();
        $category_list = $category->getCategoryList();
        $max_category = $category->getMaxCategory();
        $this->assign('category', $category_list);
        $this->assign('max_category', $max_category);
        return $this->fetch();
    }

    public function categoryInfo($id)
    {
        (new IDMustBePostiveInt())->goCheck();
        $category = new CategoryServer();
        $category_info = $category->categoryInfo($id);
        if(!$category_info) {
            return setMsg(0, '此分类没有商品');
        }
        return setMsg(1, 'OK', $category_info);
    }
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/5
 * Time: 10:58
 */

namespace app\index\controller;

use app\index\server\CategoryServer;
use app\index\validate\IDMustBePostiveInt;
use think\Controller;

class Category extends Controller
{
    public function index()
    {
        $category = new CategoryServer();
        $category_list = $category->getCategoryList();
        $max_category = $category->getMaxCategory();
        $this->assign('category', $category_list);
        $this->assign('max_category', $max_category);
        return $this->fetch();
    }

    public function categoryInfo($id)
    {
        (new IDMustBePostiveInt())->goCheck();
        $category = new CategoryServer();
        $category_info = $category->categoryInfo($id);
        if(!$category_info) {
            return setMsg(0, '此分类没有商品');
        }
        return setMsg(1, 'OK', $category_info);
    }
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}