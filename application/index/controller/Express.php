<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/10/17
 * Time: 14:20
 */

namespace app\index\controller;

use think\Controller;

class Express extends Controller
{
	static private $AppKey = "j1ivABLqx5jA";

	static private $AppSecret = "e6f07a5ece9b41cfb37ba4935d432b53";

	static private $url = "https://poll.kuaidi100.com/poll/query.do";

	static private $key = "YhXrhaBE6914";

	/**
	 * @var 请求快递数据
	 */
	static public function getExpress($express, $company)
	{
		//参数设置
		$post_data = array();
		$post_data["customer"] = "542AD8D215A515D13CB5B7FBEF4CAFE1";
		$post_data["param"] = '{"com":"' . $company . '","num":"' . $express . '"}';
		$post_data["sign"] = md5($post_data["param"] . self::$key . $post_data["customer"]);

		$post_data["sign"] = strtoupper($post_data["sign"]);
		$o = "";
		foreach ($post_data as $k => $v) {
			$o .= "$k=" . urlencode($v) . "&";        //默认UTF-8编码格式
		}
		$post_data = substr($o, 0, -1);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL, self::$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		$result = curl_exec($ch);
		$data = str_replace("\"", '"', $result);
		return $data;
	}
}