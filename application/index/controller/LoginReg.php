<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/5
 * Time: 13:58
 */

namespace app\index\controller;


use app\index\model\User as UserModel;
use app\index\server\LoginRegServer;
use app\index\server\WeChatServer;
use app\index\validate\RegValidate;
use app\lib\exception\DatabaseException;
use think\Controller;
//use think\Cookie;
use think\Exception;
use think\Request;

class LoginReg extends Controller
{
    // 用 openid 自动登陆
    public function login($code = 0)
    {
        $we_chat = new WeChatServer();
        $openid = $we_chat->Authentication();
        // 数据库查找此信息
        $user_info = UserModel::where('openid', $openid)
            ->find();
        if (!$user_info) {
            if ($code) {
                // 没有此用户则去注册
                return $this->redirect('/reg/' . $code);
            } else {
                return $this->regdirect('/reg');
            }
        }
        session('Auth.Uid', $user_info->id);
//        session('Auth.Username', $user_info->username);
//        session('Auth.IsVIP', $user_info->is_vip);
        cookie('Uid', $user_info->id);
//		Cookie::set('Uid',$user_info->id,31536000*2);
//		Cookie::set('Username',$user_info->username,31536000*2);
//		Cookie::set('IsVIP',$user_info->is_vip,31536000*2);
//        $this->redirect($url);
//        $from = Request::instance()->param('from');
//        echo '24';die;
//        if ($from == 'cart') {
//            return $this->redirect('/cart');
//        } else {
            return $this->redirect('/myself');
//        }
    }

    public function loginCode($syuid)
    {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/login/' . $syuid;
        $we_chat = new WeChatServer();
        $info = $we_chat->getUserInfo($url);
        $url = $baseUrl = urlencode('http://' . $_SERVER['HTTP_HOST'] . '/login/' . $syuid);
        // 数据库查找此信息
        $user_info = UserModel::where('openid', $info['openid'])
            ->find();
        if (!$user_info) {
            if ($syuid) {
                // 没有此用户则去注册
                $this->assign('icon', $info['headimgurl']);
                $this->assign('why', $syuid);
                $this->assign('openid', $info['openid']);
                return $this->fetch('login/reg');
            }
        }
        session('Auth.Uid', $user_info->id);
        session('Auth.Username', $user_info->username);
        session('Auth.IsVIP', $user_info->is_vip);
        return $this->redirect('/');
        exit;
    }

	public function regCode($code)
	{
		$this->reg_view($code);
	}

    public function reg_view()
    {
        $we_chat = new WeChatServer();
        $url = $baseUrl = urlencode('http://' . $_SERVER['HTTP_HOST'] . '/reg');
        $userinfo = $we_chat->getUserInfo($url);
        $this->assign('why', 0);
        $this->assign('openid', $userinfo['openid']);
        $this->assign('icon', $userinfo['headimgurl']);
        return $this->fetch('login/reg');
    }

    public function code_reg()
    {
        if (!session('Auth.Recode')) {
            $reg_code = input('get.recode');
            session('AUth.Recode', $reg_code);
        } else {
            $reg_code = session('Auth.Recode');
        }
        $openid = session('Auth.Openid');
        if (!$openid) {
            $we_chat = new WeChat();
            $wx_array = $we_chat->CodeReg();
            $openid = $wx_array['openid'];
        }
        // 数据库查找此信息
        $user_info = UserModel::where('openid', $openid)
            ->find();
        if ($user_info) {
            session('Auth.Recode', null);
            session('Auth.Uid', $user_info->id);
            session('Auth.Username', $user_info->username);
            session('Auth.IsVIP', $user_info->is_vip);
            return redirect('/');
        }
        session('Auth.Recode', null);
        session('Auth.Openid', $openid);
//        dump($reg_code);die;
        $this->assign('recode', $reg_code);
        return $this->fetch('login/reg');
    }

    public function reg()
    {
        $params = (new RegValidate())->goCheck();
        $register = new LoginRegServer();
        $reg_result = $register->reg($params);
        return $reg_result;
    }

    public function abc()
    {
        return $this->fetch('login/reg');
    }

    // 检测用户名是否可用
    public function check_name()
    {
        $name = input('post.username');
        $register = new LoginRegServer();
        $reg_result = $register->check_name($name);
        return setMsg($reg_result['status'], $reg_result['message']);
    }

    // 检测电话号码是否可用
    public function check_mobile()
    {
        $mobile = input('post.mobile');
        $register = new LoginRegServer();
        $reg_result = $register->check_mobile($mobile);
        return setMsg($reg_result['status'], $reg_result['message']);
    }
}
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/5
 * Time: 13:58
 */

namespace app\index\controller;


use app\index\model\User as UserModel;
use app\index\server\LoginRegServer;
use app\index\server\WeChatServer;
use app\index\validate\RegValidate;
use app\lib\exception\DatabaseException;
use think\Controller;
//use think\Cookie;
use think\Exception;
use think\Request;

class LoginReg extends Controller
{
    // 用 openid 自动登陆
    public function login($code = 0)
    {
        $we_chat = new WeChatServer();
        $openid = $we_chat->Authentication();
        // 数据库查找此信息
        $user_info = UserModel::where('openid', $openid)
            ->find();
        if (!$user_info) {
            if ($code) {
                // 没有此用户则去注册
                return $this->redirect('/reg/' . $code);
            } else {
                return $this->regdirect('/reg');
            }
        }
        session('Auth.Uid', $user_info->id);
//        session('Auth.Username', $user_info->username);
//        session('Auth.IsVIP', $user_info->is_vip);
        cookie('Uid', $user_info->id);
//		Cookie::set('Uid',$user_info->id,31536000*2);
//		Cookie::set('Username',$user_info->username,31536000*2);
//		Cookie::set('IsVIP',$user_info->is_vip,31536000*2);
//        $this->redirect($url);
//        $from = Request::instance()->param('from');
//        echo '24';die;
//        if ($from == 'cart') {
//            return $this->redirect('/cart');
//        } else {
            return $this->redirect('/myself');
//        }
    }

    public function loginCode($syuid)
    {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/login/' . $syuid;
        $we_chat = new WeChatServer();
        $info = $we_chat->getUserInfo($url);
        $url = $baseUrl = urlencode('http://' . $_SERVER['HTTP_HOST'] . '/login/' . $syuid);
        // 数据库查找此信息
        $user_info = UserModel::where('openid', $info['openid'])
            ->find();
        if (!$user_info) {
            if ($syuid) {
                // 没有此用户则去注册
                $this->assign('icon', $info['headimgurl']);
                $this->assign('why', $syuid);
                $this->assign('openid', $info['openid']);
                return $this->fetch('login/reg');
            }
        }
        session('Auth.Uid', $user_info->id);
        session('Auth.Username', $user_info->username);
        session('Auth.IsVIP', $user_info->is_vip);
        return $this->redirect('/');
        exit;
    }

	public function regCode($code)
	{
		$this->reg_view($code);
	}

    public function reg_view()
    {
        $we_chat = new WeChatServer();
        $url = $baseUrl = urlencode('http://' . $_SERVER['HTTP_HOST'] . '/reg');
        $userinfo = $we_chat->getUserInfo($url);
        $this->assign('why', 0);
        $this->assign('openid', $userinfo['openid']);
        $this->assign('icon', $userinfo['headimgurl']);
        return $this->fetch('login/reg');
    }

    public function code_reg()
    {
        if (!session('Auth.Recode')) {
            $reg_code = input('get.recode');
            session('AUth.Recode', $reg_code);
        } else {
            $reg_code = session('Auth.Recode');
        }
        $openid = session('Auth.Openid');
        if (!$openid) {
            $we_chat = new WeChat();
            $wx_array = $we_chat->CodeReg();
            $openid = $wx_array['openid'];
        }
        // 数据库查找此信息
        $user_info = UserModel::where('openid', $openid)
            ->find();
        if ($user_info) {
            session('Auth.Recode', null);
            session('Auth.Uid', $user_info->id);
            session('Auth.Username', $user_info->username);
            session('Auth.IsVIP', $user_info->is_vip);
            return redirect('/');
        }
        session('Auth.Recode', null);
        session('Auth.Openid', $openid);
//        dump($reg_code);die;
        $this->assign('recode', $reg_code);
        return $this->fetch('login/reg');
    }

    public function reg()
    {
        $params = (new RegValidate())->goCheck();
        $register = new LoginRegServer();
        $reg_result = $register->reg($params);
        return $reg_result;
    }

    public function abc()
    {
        return $this->fetch('login/reg');
    }

    // 检测用户名是否可用
    public function check_name()
    {
        $name = input('post.username');
        $register = new LoginRegServer();
        $reg_result = $register->check_name($name);
        return setMsg($reg_result['status'], $reg_result['message']);
    }

    // 检测电话号码是否可用
    public function check_mobile()
    {
        $mobile = input('post.mobile');
        $register = new LoginRegServer();
        $reg_result = $register->check_mobile($mobile);
        return setMsg($reg_result['status'], $reg_result['message']);
    }
}
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
