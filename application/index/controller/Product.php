<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4
 * Time: 14:43
 */

namespace app\index\controller;


use app\index\model\M;
use app\index\server\ProductServer;
use app\index\validate\BannerToValidate;
use app\index\validate\IDMustBePostiveInt;
use app\index\validate\ProductListValidate;
use app\index\validate\ProductSizeValidate;
use think\Controller;


class Product extends Controller
{
    /**
     * @param $id 类型id(theme_id/category_id)
     * @param int $offset 偏移量
     * @param int $limit 条数
     * @return mixed
     */
    public function index($id, $offset = 0, $limit = 6)
    {
        (new ProductListValidate())->goCheck();
        $product = new ProductServer();
        $list = $product->getProductList($id, $offset, $limit);
        if (!$list) {
            // 没有此类型的商品 一个单独的空页面
            return $this->fetch();
        }
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function themeProductJson()
    {
        $product = new ProductServer();
        $params = request()->param();
        $result = $product->themeList($params['id'], $params['page'], $params['number']);
        if (!$result) {
            return setMsg(0, '没有此信息');
        }
        return setMsg(1, 'OK', $result);
    }

	public function info($id)
    {
        // 根据传入的type key_word 获得商品的 id
        $product = new ProductServer();
        $product_info = $product->getProductInfo($id);
        $product_img = $product->getProductImg($id);
        $product_all = $product->productInfo($id);
        $product_home_info = $product->home($id);
        $product_banner = $product->getProductBanner($id);
        $product_property = $product->getProductProperty($id);
        $this->assign('product_banner', $product_banner);
        $this->assign('product_property', $product_property);
        // 商品总数量
        if (session('Auth.Username')) {
            $product_count = $product->countNum();
            if ($product_count) {
                $this->assign('product_count', $product_count);
            }
        }
        $M = new M();
        $sales_me = $M->find('sy_product','id',$id)['sales_me'];
        $product_info['sales_me'] = $sales_me;
        $this->assign('product_info', $product_info);
        $this->assign('product_img', $product_img);
        $this->assign('product_all', $product_all);
        $this->assign('home', $product_home_info);
        return $this->fetch();
    }



    // 参数包含 type(banner_type 文件中有对type的解释)  key_word
    public function toProductTheme()
    {
        $params = (new BannerToValidate())->goCheck();
        $product = new ProductServer();
        switch ($params['type']) {
            case '专题' : // 指向商品 渲染商品的详情
                $theme_id = $product->getThemeId($params['key_word']);
                // 查出专题的id
                return $this->redirect('/product?id=' . $theme_id->id . '&type=theme');
                break;
            default : // 指向商品 渲染商品的详情 默认
                // 查出商品的id
                $product_id = $product->getProductId($params['key_word']);
                return $this->redirect('/product/info?id=' . $product_id->id);
                break;
        }
    }

    public function size()
    {
        $params = (new ProductSizeValidate())->goCheck();
        $server = new ProductServer();
        $result = $server->size($params);
        return $result;
    }
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4
 * Time: 14:43
 */

namespace app\index\controller;


use app\index\model\M;
use app\index\server\ProductServer;
use app\index\validate\BannerToValidate;
use app\index\validate\IDMustBePostiveInt;
use app\index\validate\ProductListValidate;
use app\index\validate\ProductSizeValidate;
use think\Controller;


class Product extends Controller
{
    /**
     * @param $id 类型id(theme_id/category_id)
     * @param int $offset 偏移量
     * @param int $limit 条数
     * @return mixed
     */
    public function index($id, $offset = 0, $limit = 6)
    {
        (new ProductListValidate())->goCheck();
        $product = new ProductServer();
        $list = $product->getProductList($id, $offset, $limit);
        if (!$list) {
            // 没有此类型的商品 一个单独的空页面
            return $this->fetch();
        }
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function themeProductJson()
    {
        $product = new ProductServer();
        $params = request()->param();
        $result = $product->themeList($params['id'], $params['page'], $params['number']);
        if (!$result) {
            return setMsg(0, '没有此信息');
        }
        return setMsg(1, 'OK', $result);
    }

	public function info($id)
    {
        // 根据传入的type key_word 获得商品的 id
        $product = new ProductServer();
        $product_info = $product->getProductInfo($id);
        $product_img = $product->getProductImg($id);
        $product_all = $product->productInfo($id);
        $product_home_info = $product->home($id);
        $product_banner = $product->getProductBanner($id);
        $product_property = $product->getProductProperty($id);
        $this->assign('product_banner', $product_banner);
        $this->assign('product_property', $product_property);
        // 商品总数量
        if (session('Auth.Username')) {
            $product_count = $product->countNum();
            if ($product_count) {
                $this->assign('product_count', $product_count);
            }
        }
        $M = new M();
        $sales_me = $M->find('sy_product','id',$id)['sales_me'];
        $product_info['sales_me'] = $sales_me;
        $this->assign('product_info', $product_info);
        $this->assign('product_img', $product_img);
        $this->assign('product_all', $product_all);
        $this->assign('home', $product_home_info);
        return $this->fetch();
    }



    // 参数包含 type(banner_type 文件中有对type的解释)  key_word
    public function toProductTheme()
    {
        $params = (new BannerToValidate())->goCheck();
        $product = new ProductServer();
        switch ($params['type']) {
            case '专题' : // 指向商品 渲染商品的详情
                $theme_id = $product->getThemeId($params['key_word']);
                // 查出专题的id
                return $this->redirect('/product?id=' . $theme_id->id . '&type=theme');
                break;
            default : // 指向商品 渲染商品的详情 默认
                // 查出商品的id
                $product_id = $product->getProductId($params['key_word']);
                return $this->redirect('/product/info?id=' . $product_id->id);
                break;
        }
    }

    public function size()
    {
        $params = (new ProductSizeValidate())->goCheck();
        $server = new ProductServer();
        $result = $server->size($params);
        return $result;
    }
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}