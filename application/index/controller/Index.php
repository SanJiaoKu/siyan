<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/3
 * Time: 16:21
 */

namespace app\index\controller;

use app\index\server\IndexServer;
use app\index\server\ThemeServer;
use app\index\validate\IDMustBePostiveInt;
use think\cache\driver\Redis;
use think\Controller;
use app\model\OrderModel;

class Index extends Controller
{
    private $server;

    public function _initialize()
    {
        $this->server = new IndexServer();
    }

    public function index()
    {
        // 首页banner图
        $banner = $this->server->getBanners();
        // 专题的名称
        $theme = $this->server->themeNameList();
        // 3个最新的商品
        $product = $this->server->newProduct();
        // 最新的2个专题推荐
        $hot_theme = $this->server->hot();
        // 最小的 theme_id
        $max_theme = $this->server->max_theme();
        $this->assign('banner', $banner);
        $this->assign('theme', $theme);
        $this->assign('product', $product);
        $this->assign('hot_theme', $hot_theme);
        $this->assign('max_theme', $max_theme);
        return $this->fetch();
    }
    
    public function getSaleList()
    {
        $id = input('get.id');
        $result = $this->server->getSaleList();
        return $result;
    }

    public function getDigitalList()
    {   
        $id = input('get.id');
        $getDigitalList = $this->server->getDigitalList($id);

        // $this->assign('theme', $getDigitalList);
        return $getDigitalList;
        // return $this->fetch();
    }

    public function theme_product($id)
    {
        (new IDMustBePostiveInt())->goCheck();
        $theme = new ThemeServer();
        $result = $theme->themeProduct($id);
        if(!$result) {
            return setMsg(0, '非常抱歉,没有查到此信息,祝您购物愉快');
        }
        return setMsg(1, 'OK', $result);
    }

    public function qr_code()
    {
        dump(1111);die;
        $result = create_qrcode();
        echo $result;
    }


    public function test()
    {
        $orderNum = "byBL05134602971394";
        $orderInfo = (new Redis())->get($orderNum);
        $result = (new OrderModel())->createOrder($orderInfo['order'], $orderInfo['info'], $orderInfo['coupon'], $orderNum);
        var_dump($result);die;
        return $result;
    }
}
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/3
 * Time: 16:21
 */

namespace app\index\controller;

use app\index\server\IndexServer;
use app\index\server\ThemeServer;
use app\index\validate\IDMustBePostiveInt;
use think\cache\driver\Redis;
use think\Controller;
use app\model\OrderModel;

class Index extends Controller
{
    private $server;

    public function _initialize()
    {
        $this->server = new IndexServer();
    }

    public function index()
    {
        // 首页banner图
        $banner = $this->server->getBanners();
        // 专题的名称
        $theme = $this->server->themeNameList();
        // 3个最新的商品
        $product = $this->server->newProduct();
        // 最新的2个专题推荐
        $hot_theme = $this->server->hot();
        // 最小的 theme_id
        $max_theme = $this->server->max_theme();
        $this->assign('banner', $banner);
        $this->assign('theme', $theme);
        $this->assign('product', $product);
        $this->assign('hot_theme', $hot_theme);
        $this->assign('max_theme', $max_theme);
        return $this->fetch();
    }
    
    public function getSaleList()
    {
        $id = input('get.id');
        $result = $this->server->getSaleList();
        return $result;
    }

    public function getDigitalList()
    {   
        $id = input('get.id');
        $getDigitalList = $this->server->getDigitalList($id);

        // $this->assign('theme', $getDigitalList);
        return $getDigitalList;
        // return $this->fetch();
    }

    public function theme_product($id)
    {
        (new IDMustBePostiveInt())->goCheck();
        $theme = new ThemeServer();
        $result = $theme->themeProduct($id);
        if(!$result) {
            return setMsg(0, '非常抱歉,没有查到此信息,祝您购物愉快');
        }
        return setMsg(1, 'OK', $result);
    }

    public function qr_code()
    {
        dump(1111);die;
        $result = create_qrcode();
        echo $result;
    }


    public function test()
    {
        $orderNum = "byBL05134602971394";
        $orderInfo = (new Redis())->get($orderNum);
        $result = (new OrderModel())->createOrder($orderInfo['order'], $orderInfo['info'], $orderInfo['coupon'], $orderNum);
        var_dump($result);die;
        return $result;
    }
}
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
