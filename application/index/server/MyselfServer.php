<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Yangxufeng
 * Date: 2018-09-17
 * Time: 10:12
 */

namespace app\index\server;

use app\index\model\User as UserModel;
use app\index\model\Address as AddressModel;
use app\lib\exception\DatabaseException;
use think\Request;
use think\Db;

class MyselfServer extends BaseServer
{
    static private $length = 50;

    // 获取用户的个人信息
    public function getUserInfo($id)
    {
        try {
            $user_info = UserModel::where('id', '=', $id)
                ->where('status', '=', 1)
                ->find();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40050,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $user_info;
    }

    // 提现功能
    public function getTixInfo()
    {
        try {
            $user_info = UserModel::where('id','=',session('Auth.Uid'))
                ->field('balance')
                ->find();


        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
        return $user_info;
    }

    public function getSonInfo($condition, $limit)
    {
        $offset = $limit * self::$length;
        try {
            $result = UserModel::
            where($condition)
            ->where('status', 1)
            ->field('username, mobile, id')
            // ->limit($offset, self::$length)
            ->with(['subUsers'=>function($query){
                $query->field('username,mobile,p_id,id');
            }])
            ->order('id desc')
            ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                                        'errorCode' => 40050,
                                        'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
                                        ]);
        }
        return $result;
    }

    public function getSonInfo2($condition, $limit)
    {
        $result = UserModel::where($condition)
                            ->where('status', '=', 1)
                            ->field('username, mobile')
                            ->order('id desc')
                            ->select();


    }


    // 获取地址列表
    public function getAddress($uid)
    {
        try {
            $addr_info = AddressModel::where('user_id', '=', $uid)
                ->where('status', '=', 1)
                ->limit(0, 3)
                ->select();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40051,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $addr_info;
    }

    public function insertAddress()
    {
        $params = Request::instance()->param();
        try {
            $user_id = $params['user_id'];
            $count = AddressModel::where('status', '=', 1)
                ->where('user_id', '=', $user_id)
                ->count();
            if($count >= 3){
                return setMsg(0, '收货地址最多只能保存3条');
            }
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40052,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        Db::startTrans();
        try {
            if($count != 0){
                $result = AddressModel::where('status', '=', 1)
                    ->where('user_id', '=', $user_id)
                    ->update(['default_address' => 0]);
            }
            // 切割收货地址
            $array = explode(' ', $params['address']);
            $data = [
                'consignee' => $params['name'],
                'mobile' => $params['phone'],
                'province' => $array[0],
                'city' => $array[1],
                'country' => $array[2],
                'detail' =>$params['addressDetail'],
                'user_id' => $params['user_id'],
                'default_address' => 1
            ];
            $insId = AddressModel::insertGetId($data);
            Db::commit();
            $data['id'] = $insId;
        } catch(Exception $e) {
            Db::rollback();
            throw new DatabaseException([
                'errorCode' => 40053,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return setMsg(1, '添加成功', $data);
    }

    public function changeAddress()
    {
        $params = Request::instance()->param();
        Db::startTrans();
        try{
            $result = AddressModel::where('user_id', '=', $params['user_id'])
                ->where('status', '=', 1)
                ->update(['default_address' => 0]);
            $info = AddressModel::where('id', '=', $params['id'])->find();
            $info->default_address = abs( ($info->default_address) - 1 );
            $result = $info->save();
            Db::commit();
        }catch(Exception $e){
            Db::rollback();
            throw new DatabaseException([
                'errorCode' => 40059,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return setMsg(1, '变更成功');
    }

    // 收货地址详情
    public function detail($id)
    {
        try {
            $data = AddressModel::where('id', '=', $id)
                ->where('status', '=', 1)
                ->find();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40060,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $data;
    }

    // 更新地址信息
    public function update($params)
    {
        $array = explode(' ', $params['address']);
        $data = [
            'consignee' => $params['name'],
            'mobile' => $params['phone'],
            'province' => $array[0],
            'city' => $array[1],
            'country' => $array[2],
            'detail' =>$params['addressDetail'],
        ];
        try {
            $id = $params['id'];
            $user_id = $params['user_id'];
            $result = AddressModel::where('id', '=', $id)
                ->where('user_id', '=', $user_id)
                ->update($data);
            if(!$result){
                return setMsg(0, '请更改收货地址');
            }
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40061,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        $data['id'] = $params['id'];
        return setMsg(1, '更新成功', $data);
    }

    // 删除指定id的收货地址
    public function delAddress($id)
    {
        $info = AddressModel::where('id', '=', $id)->find();
        if($info->default_address == 1){
            return setMsg(0, '无法删除默认收货地址');
        }
        $info->status = abs( ($info->status) - 1 );
        $result = $info->save();
        if(!$result){
            return setMsg(0, '删除收货地址失败');
        }
        return setMsg(1, '删除收货地址成功');
    }

    public function obtainUserInfo($condition, $field)
    {
        try {
            $user_info = UserModel::where($condition)
                ->where('status', '=', 1)
                ->field($field)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40057,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $user_info;
    }

    public function changeUserInfo($condition, $arr)
    {
        try {
            $result = UserModel::where($condition)
                ->update($arr);
            if (!$result) {
                return setMsg(0, '修改失败');
            }
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40058,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return setMsg(1, '修改成功');
    }

    public function getValue($condition, $field)
    {
        try {
            $value = UserModel::where($condition)
            ->value($field);
        } catch (Exception $e) {
            throw new DatabaseException([
                                        'errorCode' => 40058,
                                        'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
                                        ]);
        }
        return $value;
    }
}
=======
<?php
/**
 * Created by PhpStorm.
 * User: Yangxufeng
 * Date: 2018-09-17
 * Time: 10:12
 */

namespace app\index\server;

use app\index\model\User as UserModel;
use app\index\model\Address as AddressModel;
use app\lib\exception\DatabaseException;
use think\Request;
use think\Db;

class MyselfServer extends BaseServer
{
    static private $length = 50;

    // 获取用户的个人信息
    public function getUserInfo($id)
    {
        try {
            $user_info = UserModel::where('id', '=', $id)
                ->where('status', '=', 1)
                ->find();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40050,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $user_info;
    }

    // 提现功能
    public function getTixInfo()
    {
        try {
            $user_info = UserModel::where('id','=',session('Auth.Uid'))
                ->field('balance')
                ->find();


        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
        return $user_info;
    }

    public function getSonInfo($condition, $limit)
    {
        $offset = $limit * self::$length;
        try {
            $result = UserModel::
            where($condition)
            ->where('status', 1)
            ->field('username, mobile, id')
            // ->limit($offset, self::$length)
            ->with(['subUsers'=>function($query){
                $query->field('username,mobile,p_id,id');
            }])
            ->order('id desc')
            ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                                        'errorCode' => 40050,
                                        'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
                                        ]);
        }
        return $result;
    }

    public function getSonInfo2($condition, $limit)
    {
        $result = UserModel::where($condition)
                            ->where('status', '=', 1)
                            ->field('username, mobile')
                            ->order('id desc')
                            ->select();


    }


    // 获取地址列表
    public function getAddress($uid)
    {
        try {
            $addr_info = AddressModel::where('user_id', '=', $uid)
                ->where('status', '=', 1)
                ->limit(0, 3)
                ->select();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40051,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $addr_info;
    }

    public function insertAddress()
    {
        $params = Request::instance()->param();
        try {
            $user_id = $params['user_id'];
            $count = AddressModel::where('status', '=', 1)
                ->where('user_id', '=', $user_id)
                ->count();
            if($count >= 3){
                return setMsg(0, '收货地址最多只能保存3条');
            }
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40052,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        Db::startTrans();
        try {
            if($count != 0){
                $result = AddressModel::where('status', '=', 1)
                    ->where('user_id', '=', $user_id)
                    ->update(['default_address' => 0]);
            }
            // 切割收货地址
            $array = explode(' ', $params['address']);
            $data = [
                'consignee' => $params['name'],
                'mobile' => $params['phone'],
                'province' => $array[0],
                'city' => $array[1],
                'country' => $array[2],
                'detail' =>$params['addressDetail'],
                'user_id' => $params['user_id'],
                'default_address' => 1
            ];
            $insId = AddressModel::insertGetId($data);
            Db::commit();
            $data['id'] = $insId;
        } catch(Exception $e) {
            Db::rollback();
            throw new DatabaseException([
                'errorCode' => 40053,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return setMsg(1, '添加成功', $data);
    }

    public function changeAddress()
    {
        $params = Request::instance()->param();
        Db::startTrans();
        try{
            $result = AddressModel::where('user_id', '=', $params['user_id'])
                ->where('status', '=', 1)
                ->update(['default_address' => 0]);
            $info = AddressModel::where('id', '=', $params['id'])->find();
            $info->default_address = abs( ($info->default_address) - 1 );
            $result = $info->save();
            Db::commit();
        }catch(Exception $e){
            Db::rollback();
            throw new DatabaseException([
                'errorCode' => 40059,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return setMsg(1, '变更成功');
    }

    // 收货地址详情
    public function detail($id)
    {
        try {
            $data = AddressModel::where('id', '=', $id)
                ->where('status', '=', 1)
                ->find();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40060,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $data;
    }

    // 更新地址信息
    public function update($params)
    {
        $array = explode(' ', $params['address']);
        $data = [
            'consignee' => $params['name'],
            'mobile' => $params['phone'],
            'province' => $array[0],
            'city' => $array[1],
            'country' => $array[2],
            'detail' =>$params['addressDetail'],
        ];
        try {
            $id = $params['id'];
            $user_id = $params['user_id'];
            $result = AddressModel::where('id', '=', $id)
                ->where('user_id', '=', $user_id)
                ->update($data);
            if(!$result){
                return setMsg(0, '请更改收货地址');
            }
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40061,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        $data['id'] = $params['id'];
        return setMsg(1, '更新成功', $data);
    }

    // 删除指定id的收货地址
    public function delAddress($id)
    {
        $info = AddressModel::where('id', '=', $id)->find();
        if($info->default_address == 1){
            return setMsg(0, '无法删除默认收货地址');
        }
        $info->status = abs( ($info->status) - 1 );
        $result = $info->save();
        if(!$result){
            return setMsg(0, '删除收货地址失败');
        }
        return setMsg(1, '删除收货地址成功');
    }

    public function obtainUserInfo($condition, $field)
    {
        try {
            $user_info = UserModel::where($condition)
                ->where('status', '=', 1)
                ->field($field)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40057,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $user_info;
    }

    public function changeUserInfo($condition, $arr)
    {
        try {
            $result = UserModel::where($condition)
                ->update($arr);
            if (!$result) {
                return setMsg(0, '修改失败');
            }
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40058,
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return setMsg(1, '修改成功');
    }

    public function getValue($condition, $field)
    {
        try {
            $value = UserModel::where($condition)
            ->value($field);
        } catch (Exception $e) {
            throw new DatabaseException([
                                        'errorCode' => 40058,
                                        'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
                                        ]);
        }
        return $value;
    }
}
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
