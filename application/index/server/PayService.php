<<<<<<< HEAD
<?php
/**
 * Created by 七月.
 * Author: 七月
 * Date: 2017/6/1
 * Time: 17:08
 */

namespace app\index\server;

use app\index\model\Order as OrderModel;
use think\Exception;
use think\Loader;
use think\Log;
use think\Db;

//   extend/WxPay/WxPay.Api.php
Loader::import('WxPay.WxPay', EXTEND_PATH, '.Api.php');

class PayService
{
//    private $orderID;
//    private $orderNO;

//    function __construct()
//    {
//        // 获取微信订单号
//        $this->orderNO = billno();
//    }

    public function pay($openid, $price, $order_no, $from = '')
    {
        return $this->makeWxPreOrder($openid, $price, $order_no, '/');
    }

    private function makeWxPreOrder($openid, $totalPrice, $order_no, $from = '')
    {
        if (!$openid) {
            throw new Exception('openid 丢失');
        }
	$wxOrderData = new \WxPayUnifiedOrder();
        $wxOrderData->SetOut_trade_no($order_no);
        $wxOrderData->SetTrade_type('JSAPI');
        $wxOrderData->SetTotal_fee($totalPrice * 100);
        $wxOrderData->SetBody('源之东方订单支付');
        $wxOrderData->SetOpenid($openid);
        if($from == 'member') {
            $wxOrderData->SetNotify_url(config('secure.member_back_url'));
//            dump(config('secure.member_back_url'));die;
        } else {
            $wxOrderData->SetNotify_url(config('secure.pay_back_url'));
//            dump(config('secure.pay_back_url'));die;
        }
        return $this->getPaySignature($wxOrderData);
    }

    private function getPaySignature($wxOrderData)
    {
        $wxOrder = \WxPayApi::unifiedOrder($wxOrderData);
        if ($wxOrder['return_code'] != 'SUCCESS' || $wxOrder['result_code'] != 'SUCCESS') {
            Log::record($wxOrder, 'error');
            Log::record('获取预支付订单失败', 'error');
        }
        $signature = $this->sign($wxOrder);
        return $signature;
    }

    private function sign($wxOrder)
    {
        $jsApiPayData = new \WxPayJsApiPay();
        $jsApiPayData->SetAppid(config('wechat.AppId'));
        $jsApiPayData->SetTimeStamp((string)time());

        $rand = md5(time() . mt_rand(0, 1000));
        $jsApiPayData->SetNonceStr($rand);
        // dump($wxOrder['prepay_id']);
        $jsApiPayData->SetPackage('prepay_id='.$wxOrder['prepay_id']);
        $jsApiPayData->SetSignType('md5');

        $sign = $jsApiPayData->MakeSign();
        $rawValues = $jsApiPayData->GetValues();
        $rawValues['paySign'] = $sign;
        return $rawValues;
    }

//    private function recordPreOrder($wxOrder)
//    {
//        OrderModel::where('id', '=', $this->orderID)
//            ->update(['prepay_id' => $wxOrder['prepay_id']]);
//    }
}
=======
<?php
/**
 * Created by 七月.
 * Author: 七月
 * Date: 2017/6/1
 * Time: 17:08
 */

namespace app\index\server;

use app\index\model\Order as OrderModel;
use think\Exception;
use think\Loader;
use think\Log;
use think\Db;

//   extend/WxPay/WxPay.Api.php
Loader::import('WxPay.WxPay', EXTEND_PATH, '.Api.php');

class PayService
{
//    private $orderID;
//    private $orderNO;

//    function __construct()
//    {
//        // 获取微信订单号
//        $this->orderNO = billno();
//    }

    public function pay($openid, $price, $order_no, $from = '')
    {
        return $this->makeWxPreOrder($openid, $price, $order_no, '/');
    }

    private function makeWxPreOrder($openid, $totalPrice, $order_no, $from = '')
    {
        if (!$openid) {
            throw new Exception('openid 丢失');
        }
	$wxOrderData = new \WxPayUnifiedOrder();
        $wxOrderData->SetOut_trade_no($order_no);
        $wxOrderData->SetTrade_type('JSAPI');
        $wxOrderData->SetTotal_fee($totalPrice * 100);
        $wxOrderData->SetBody('源之东方订单支付');
        $wxOrderData->SetOpenid($openid);
        if($from == 'member') {
            $wxOrderData->SetNotify_url(config('secure.member_back_url'));
//            dump(config('secure.member_back_url'));die;
        } else {
            $wxOrderData->SetNotify_url(config('secure.pay_back_url'));
//            dump(config('secure.pay_back_url'));die;
        }
        return $this->getPaySignature($wxOrderData);
    }

    private function getPaySignature($wxOrderData)
    {
        $wxOrder = \WxPayApi::unifiedOrder($wxOrderData);
        if ($wxOrder['return_code'] != 'SUCCESS' || $wxOrder['result_code'] != 'SUCCESS') {
            Log::record($wxOrder, 'error');
            Log::record('获取预支付订单失败', 'error');
        }
        $signature = $this->sign($wxOrder);
        return $signature;
    }

    private function sign($wxOrder)
    {
        $jsApiPayData = new \WxPayJsApiPay();
        $jsApiPayData->SetAppid(config('wechat.AppId'));
        $jsApiPayData->SetTimeStamp((string)time());

        $rand = md5(time() . mt_rand(0, 1000));
        $jsApiPayData->SetNonceStr($rand);
        // dump($wxOrder['prepay_id']);
        $jsApiPayData->SetPackage('prepay_id='.$wxOrder['prepay_id']);
        $jsApiPayData->SetSignType('md5');

        $sign = $jsApiPayData->MakeSign();
        $rawValues = $jsApiPayData->GetValues();
        $rawValues['paySign'] = $sign;
        return $rawValues;
    }

//    private function recordPreOrder($wxOrder)
//    {
//        OrderModel::where('id', '=', $this->orderID)
//            ->update(['prepay_id' => $wxOrder['prepay_id']]);
//    }
}
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
