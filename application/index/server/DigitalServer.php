<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/3
 * Time: 21:15
 */

namespace app\index\server;


use app\index\model\Product;
use app\index\model\Theme as ThemeModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class DigitalServer extends BaseServer
{
    public function getList()
    {   
        try {
            $user_info = Product::where('status', '=', 1)
                ->where('cp_hot', '=', 1)
                ->order('id', 'desc')
                ->limit(50)
                ->select();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
        return $user_info;
    }

    public function themeNameList()
    {
        try {
            $theme_name = ThemeModel::where('status', '=', 1)
                ->where('cpp_hot', '=', 1)
                ->field('name,id')
                ->order('id', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $theme_name;
    }


    // 查询最小的一个theme 前台默认选中需要
    public function max_theme()
    {
        try {
            $min_id = ThemeModel::where('status', '=', 1)
                ->where('is_hot', '=', 0)
                ->order('id', 'desc')
                ->limit(1)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20005',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $min_id->id;
    }

=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/3
 * Time: 21:15
 */

namespace app\index\server;


use app\index\model\Product;
use app\index\model\Theme as ThemeModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class DigitalServer extends BaseServer
{
    public function getList()
    {   
        try {
            $user_info = Product::where('status', '=', 1)
                ->where('cp_hot', '=', 1)
                ->order('id', 'desc')
                ->limit(50)
                ->select();
        } catch(Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
        return $user_info;
    }

    public function themeNameList()
    {
        try {
            $theme_name = ThemeModel::where('status', '=', 1)
                ->where('cpp_hot', '=', 1)
                ->field('name,id')
                ->order('id', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $theme_name;
    }


    // 查询最小的一个theme 前台默认选中需要
    public function max_theme()
    {
        try {
            $min_id = ThemeModel::where('status', '=', 1)
                ->where('is_hot', '=', 0)
                ->order('id', 'desc')
                ->limit(1)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20005',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $min_id->id;
    }

>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}