<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4
 * Time: 9:22
 */

namespace app\index\server;

use app\index\model\BannerItem as BannerItemModel;
use app\index\model\Product as ProductModel;
use app\index\model\Theme as ThemeModel;
use app\index\model\Category as CategoryModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class IndexServer extends BaseServer
{
    public function getBanners()
    {
        try {
            $banner_item = BannerItemModel::with(['img'])
                ->where('banner_id', config('index.first_banner'))
                ->where('status', '=', 1)
                ->order('id', 'desc')
                ->limit(5)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '10001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        } 
        return $banner_item;
    }
    
    public function getSaleList()
    {
        try {
            $result = ProductModel::where('sale_id', '>', 0)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    public function themeNameList()
    {
        try {
            $theme_name = ThemeModel::where('status', '=', 1)
                ->where('is_hot', '=', 0)
                ->field('name,id')
                ->order('id', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $theme_name;
    }
    //  手机数码---------------------
    public function getCategory()
    {
        try{
            $category_name = CategoryModel::where('status', '=', 1)
                ->field('id, name')
                ->select();
        }catch(Exception $e){
            throw new DatabaseException([
                'errorCode' => '20002',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $category_name;
    }

    public function getDigitalList($id)
    {   
        
        try {
            $theme_name = ThemeModel::with(['products'])
                ->where('status', '=', 1)
                ->where('cpp_hot', '=', $id)
                ->field('name,id')
                ->order('id', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $theme_name;
    }
    //  手机数码---------------------

    // 最新的3个商品
    public function newProduct()
    {
        try {
            $product = ProductModel::where('status', '=', 1)
                ->order('id', 'desc')
                ->limit(4)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '30001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);

        }
        return $product;
    }

    public function hot()
    {
        try {
            $hot = ThemeModel::with(['headImg'])
                ->where('status', '=', 1)
                ->where('is_hot', '=', 1)
                ->order('id', 'desc')
                ->limit(2)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20003',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $hot;
    }

    // 查询最小的一个theme 前台默认选中需要
    public function max_theme()
    {
        try {
            $min_id = ThemeModel::where('status', '=', 1)
                ->where('is_hot', '=', 0)
                ->order('id', 'desc')
                ->limit(1)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20005',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $min_id->id;
    }
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4
 * Time: 9:22
 */

namespace app\index\server;

use app\index\model\BannerItem as BannerItemModel;
use app\index\model\Product as ProductModel;
use app\index\model\Theme as ThemeModel;
use app\index\model\Category as CategoryModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class IndexServer extends BaseServer
{
    public function getBanners()
    {
        try {
            $banner_item = BannerItemModel::with(['img'])
                ->where('banner_id', config('index.first_banner'))
                ->where('status', '=', 1)
                ->order('id', 'desc')
                ->limit(5)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '10001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        } 
        return $banner_item;
    }
    
    public function getSaleList()
    {
        try {
            $result = ProductModel::where('sale_id', '>', 0)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '',
                'msg' => $e->getMessage()
            ]);
        }
        return $result;
    }

    public function themeNameList()
    {
        try {
            $theme_name = ThemeModel::where('status', '=', 1)
                ->where('is_hot', '=', 0)
                ->field('name,id')
                ->order('id', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $theme_name;
    }
    //  手机数码---------------------
    public function getCategory()
    {
        try{
            $category_name = CategoryModel::where('status', '=', 1)
                ->field('id, name')
                ->select();
        }catch(Exception $e){
            throw new DatabaseException([
                'errorCode' => '20002',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $category_name;
    }

    public function getDigitalList($id)
    {   
        
        try {
            $theme_name = ThemeModel::with(['products'])
                ->where('status', '=', 1)
                ->where('cpp_hot', '=', $id)
                ->field('name,id')
                ->order('id', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $theme_name;
    }
    //  手机数码---------------------

    // 最新的3个商品
    public function newProduct()
    {
        try {
            $product = ProductModel::where('status', '=', 1)
                ->order('id', 'desc')
                ->limit(4)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '30001',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);

        }
        return $product;
    }

    public function hot()
    {
        try {
            $hot = ThemeModel::with(['headImg'])
                ->where('status', '=', 1)
                ->where('is_hot', '=', 1)
                ->order('id', 'desc')
                ->limit(2)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20003',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $hot;
    }

    // 查询最小的一个theme 前台默认选中需要
    public function max_theme()
    {
        try {
            $min_id = ThemeModel::where('status', '=', 1)
                ->where('is_hot', '=', 0)
                ->order('id', 'desc')
                ->limit(1)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => '20005',
                'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
            ]);
        }
        return $min_id->id;
    }
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}