<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/5
 * Time: 10:59
 */

namespace app\index\server;


use app\index\model\Category as CategoryModel;
use app\index\model\Product as ProductModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class CategoryServer extends BaseServer
{
    public function getCategoryList()
    {
        // 获取名称和 id 即可
        try {
            $category = CategoryModel::where('status', '=', 1)
                ->order('id', 'desc')
                ->field('id,name')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40001,
                'msg' => '查询分类信息出错'
            ]);
        }
        return $category;
    }

    public function categoryInfo($id)
    {
        try {
            $category_info = ProductModel::with(['img'])
                ->where('status', '=', 1)
                ->where('category_id', $id)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 30022,
                'msg' => '查询分类信息出错'
            ]);
        }
        return $category_info;
    }

    public function getMaxCategory()
    {
        try {
            $min_cat = CategoryModel::where('status', '=', 1)
                ->order('id', 'desc')
                ->limit(1)
                ->field('id,name')
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => '查询最小分类出错'
            ]);
        }
        return $min_cat->id;
    }
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/5
 * Time: 10:59
 */

namespace app\index\server;


use app\index\model\Category as CategoryModel;
use app\index\model\Product as ProductModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class CategoryServer extends BaseServer
{
    public function getCategoryList()
    {
        // 获取名称和 id 即可
        try {
            $category = CategoryModel::where('status', '=', 1)
                ->order('id', 'desc')
                ->field('id,name')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 40001,
                'msg' => '查询分类信息出错'
            ]);
        }
        return $category;
    }

    public function categoryInfo($id)
    {
        try {
            $category_info = ProductModel::with(['img'])
                ->where('status', '=', 1)
                ->where('category_id', $id)
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 30022,
                'msg' => '查询分类信息出错'
            ]);
        }
        return $category_info;
    }

    public function getMaxCategory()
    {
        try {
            $min_cat = CategoryModel::where('status', '=', 1)
                ->order('id', 'desc')
                ->limit(1)
                ->field('id,name')
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'msg' => '查询最小分类出错'
            ]);
        }
        return $min_cat->id;
    }
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}