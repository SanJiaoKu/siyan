<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4
 * Time: 10:03
 */

namespace app\index\server;

use app\index\model\Theme as ThemeModel;
use app\index\model\Product as ProductModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class ThemeServer extends BaseServer
{
    // 根据一个theme_id查询这个里面的所有的商品
    public function themeProduct($id)
    {
        try {
            $theme_product = ThemeModel::with(['products'])
                ->where('status', '=', 1)
                ->where('id', $id)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 20002,
                'msg' => '非常抱歉,查询是发生了错误,给您带来的不便请谅解,请联系在线管理员,谢谢您的配合'
            ]);
        }
        return $theme_product;
    }

    public function getThemeList()
    {
        try {
            $theme_list = ThemeModel::with(['topicImg', 'headImg'])
                ->where('status', '=', 1)
                ->order('is_hot', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 20003,
                'msg' => '抱歉，查询时发生错误,请联系管理员'
            ]);
        }
        return $theme_list;
    }

    // 获取指定主题的商品列表
    public function getProductList($theme_id)
    {
        try {
//            ProductModel::with([''])
        } catch(Exception $e) {

        }
    }
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4
 * Time: 10:03
 */

namespace app\index\server;

use app\index\model\Theme as ThemeModel;
use app\index\model\Product as ProductModel;
use app\lib\exception\DatabaseException;
use think\Exception;

class ThemeServer extends BaseServer
{
    // 根据一个theme_id查询这个里面的所有的商品
    public function themeProduct($id)
    {
        try {
            $theme_product = ThemeModel::with(['products'])
                ->where('status', '=', 1)
                ->where('id', $id)
                ->find();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 20002,
                'msg' => '非常抱歉,查询是发生了错误,给您带来的不便请谅解,请联系在线管理员,谢谢您的配合'
            ]);
        }
        return $theme_product;
    }

    public function getThemeList()
    {
        try {
            $theme_list = ThemeModel::with(['topicImg', 'headImg'])
                ->where('status', '=', 1)
                ->order('is_hot', 'desc')
                ->select();
        } catch (Exception $e) {
            throw new DatabaseException([
                'errorCode' => 20003,
                'msg' => '抱歉，查询时发生错误,请联系管理员'
            ]);
        }
        return $theme_list;
    }

    // 获取指定主题的商品列表
    public function getProductList($theme_id)
    {
        try {
//            ProductModel::with([''])
        } catch(Exception $e) {

        }
    }
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
}