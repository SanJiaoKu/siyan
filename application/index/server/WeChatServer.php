<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/25
 * Time: 10:52
 */

namespace app\index\server;


use think\Db;

class WeChatServer
{
    // 认证微信
    public function Authentication()
    {
        if (!isset($_GET['code'])){
//	   $baseUrl = urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING']);
			$baseUrl = urlencode('http://sy.58bjcf.com/login');
            $url = config('wechat.AuthUrl') . 'connect/oauth2/authorize?appid=' . config('wechat.AppId') .'&redirect_uri=' . $baseUrl .'&response_type=code&scope=snsapi_base&state=123#wechat_redirect';
            header('Location:' . $url);
            exit;
        } else {
            //获取code码，以获取openid
            $code = $_GET['code'];
            $openid = $this->getOpenId($code);
            return $openid;
        }
    }

	public function myou($urlpo)
	{
		if (!isset($_GET['code'])) {
			$baseUrl = urlencode($urlpo);
			$url = config('wechat.AuthUrl') . 'connect/oauth2/authorize?appid=' . config('wechat.AppId') . '&redirect_uri=' . $baseUrl . '&response_type=code&scope=snsapi_base&state=123#wechat_redirect';
			header('Location:' . $url);
			exit;
		} else {
			//获取code码，以获取openid
			$code = $_GET['code'];
			$openid = $this->getAccessToken($code);
            return $openid['openid'];
		}
	}

    public function getOpenId($code)
    {
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . config('wechat.AppId') . '&secret=' . config('wechat.AppSecret') . '&code=' . $code . '&grant_type=authorization_code';
        $res = $this->http_curl($url);
        // 获取到了 openid
        // 可以通过openid 去拉去微信支付
        $openid = $res['openid'];
        return $openid;
    }

    public function getUserinfo($baseUrl)
    {
	if (!isset($_GET['code'])){
            $url = config('wechat.AuthUrl') . 'connect/oauth2/authorize?appid=' . config('wechat.AppId') .'&redirect_uri=' . $baseUrl .'&response_type=code&scope=snsapi_userinfo&state=state#wechat_redirect';
            header('Location:' . $url);
            exit;
        } else {
	    //获取code码，以获取openid和access_token                  
	    $code = $_GET["code"];
	    $res = $this->getAccessToken($code);
            $openid = $res['openid'];
	    $access_token = $res["access_token"];
        if ($openid && $access_token) {
            $info = $this->getInfo($openid, $access_token);
           return $info;
        } else {
	    $this->redirect('/');
	}
	    //通过openid和access_token获取登录的用户信息
	    
	    
        }

    }
    
    // 我的请求
    public function getInfo($openid, $access_token)
    {
        $url="https://api.weixin.qq.com/sns/userinfo?access_token=".$access_token."&openid=".$openid."&lang=zh_CN";
        $data['openid']=$openid;
        $data['access_token']=$access_token;
        $result = $this->http_post($url, $data);
        $result = json_decode($result, true);
        return $result;
    }
    
    public function http_post($url, $data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    //获取openid 和 access_token
    private function getAccessToken($code)
    {
       $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" . config('wechat.AppId') . "&secret=" . config('wechat.AppSecret') . "&code=" . $code . "&grant_type=authorization_code";
        $result = $this->http_curl($url);
        return $result;
    }
    
    public function getOpenidFromMp($code)
    {
        $url='https://api.weixin.qq.com/sns/oauth2/access_token?appid='.config('wechat.AppId').'&secret='.config('wechat.AppSecret').'&code='.$code.'&grant_type=authorization_code';
#       $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . config('wechat.AppId') . '&secret=' . config('wechat.AppSecret') . '&code=' . $code . '&grant_type=authorization_code';
        $res = $this->http_curl($url);
        // 获取到了 openid
        // 可以通过openid 去拉去微信支付
        $openid = $res['openid'];
        return $openid;
    }

    public function is_pay()
    {
        $openid = $this->Authentication();
        $price = 0.01;
        $piaoshu = input('post.piaoshu');  //  购买的票数
        $pid = input("post.pid"); // 选手id
        // 封装数组
        $data= [
            'piaoshu'=>$piaoshu,  // 购买的总票数
            'pid'=>$pid  // 选手id
        ];
        $pay = new Pay();
        $result = $pay->getPreOrder($openid, $price, $data);

        return $result;
    }

    public function http_curl($url){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output,true);
    }

    public function call()
    {
        //通知频率为15/15/30/180/1800/1800/1800/1800/3600，单位：秒
        // 如果成功处理，我们返回微信成功处理的信息。否则，我们需要返回没有成功处理。
        //特点：post；xml格式；不会携带参数
        $notify = new WxNotify();
        $notify->Handle();
    }
}
=======
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/25
 * Time: 10:52
 */

namespace app\index\server;


use think\Db;

class WeChatServer
{
    // 认证微信
    public function Authentication()
    {
        if (!isset($_GET['code'])){
//	   $baseUrl = urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING']);
			$baseUrl = urlencode('http://sy.58bjcf.com/login');
            $url = config('wechat.AuthUrl') . 'connect/oauth2/authorize?appid=' . config('wechat.AppId') .'&redirect_uri=' . $baseUrl .'&response_type=code&scope=snsapi_base&state=123#wechat_redirect';
            header('Location:' . $url);
            exit;
        } else {
            //获取code码，以获取openid
            $code = $_GET['code'];
            $openid = $this->getOpenId($code);
            return $openid;
        }
    }

	public function myou($urlpo)
	{
		if (!isset($_GET['code'])) {
			$baseUrl = urlencode($urlpo);
			$url = config('wechat.AuthUrl') . 'connect/oauth2/authorize?appid=' . config('wechat.AppId') . '&redirect_uri=' . $baseUrl . '&response_type=code&scope=snsapi_base&state=123#wechat_redirect';
			header('Location:' . $url);
			exit;
		} else {
			//获取code码，以获取openid
			$code = $_GET['code'];
			$openid = $this->getAccessToken($code);
            return $openid['openid'];
		}
	}

    public function getOpenId($code)
    {
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . config('wechat.AppId') . '&secret=' . config('wechat.AppSecret') . '&code=' . $code . '&grant_type=authorization_code';
        $res = $this->http_curl($url);
        // 获取到了 openid
        // 可以通过openid 去拉去微信支付
        $openid = $res['openid'];
        return $openid;
    }

    public function getUserinfo($baseUrl)
    {
	if (!isset($_GET['code'])){
            $url = config('wechat.AuthUrl') . 'connect/oauth2/authorize?appid=' . config('wechat.AppId') .'&redirect_uri=' . $baseUrl .'&response_type=code&scope=snsapi_userinfo&state=state#wechat_redirect';
            header('Location:' . $url);
            exit;
        } else {
	    //获取code码，以获取openid和access_token                  
	    $code = $_GET["code"];
	    $res = $this->getAccessToken($code);
            $openid = $res['openid'];
	    $access_token = $res["access_token"];
        if ($openid && $access_token) {
            $info = $this->getInfo($openid, $access_token);
           return $info;
        } else {
	    $this->redirect('/');
	}
	    //通过openid和access_token获取登录的用户信息
	    
	    
        }

    }
    
    // 我的请求
    public function getInfo($openid, $access_token)
    {
        $url="https://api.weixin.qq.com/sns/userinfo?access_token=".$access_token."&openid=".$openid."&lang=zh_CN";
        $data['openid']=$openid;
        $data['access_token']=$access_token;
        $result = $this->http_post($url, $data);
        $result = json_decode($result, true);
        return $result;
    }
    
    public function http_post($url, $data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    //获取openid 和 access_token
    private function getAccessToken($code)
    {
       $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" . config('wechat.AppId') . "&secret=" . config('wechat.AppSecret') . "&code=" . $code . "&grant_type=authorization_code";
        $result = $this->http_curl($url);
        return $result;
    }
    
    public function getOpenidFromMp($code)
    {
        $url='https://api.weixin.qq.com/sns/oauth2/access_token?appid='.config('wechat.AppId').'&secret='.config('wechat.AppSecret').'&code='.$code.'&grant_type=authorization_code';
#       $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . config('wechat.AppId') . '&secret=' . config('wechat.AppSecret') . '&code=' . $code . '&grant_type=authorization_code';
        $res = $this->http_curl($url);
        // 获取到了 openid
        // 可以通过openid 去拉去微信支付
        $openid = $res['openid'];
        return $openid;
    }

    public function is_pay()
    {
        $openid = $this->Authentication();
        $price = 0.01;
        $piaoshu = input('post.piaoshu');  //  购买的票数
        $pid = input("post.pid"); // 选手id
        // 封装数组
        $data= [
            'piaoshu'=>$piaoshu,  // 购买的总票数
            'pid'=>$pid  // 选手id
        ];
        $pay = new Pay();
        $result = $pay->getPreOrder($openid, $price, $data);

        return $result;
    }

    public function http_curl($url){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output,true);
    }

    public function call()
    {
        //通知频率为15/15/30/180/1800/1800/1800/1800/3600，单位：秒
        // 如果成功处理，我们返回微信成功处理的信息。否则，我们需要返回没有成功处理。
        //特点：post；xml格式；不会携带参数
        $notify = new WxNotify();
        $notify->Handle();
    }
}
>>>>>>> 93c40d8086b7a8d5a9aa3548fdc8eb06a4272cf7
