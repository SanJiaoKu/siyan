<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/4
 * Time: 8:50
 */

namespace app\index\model;
use think\Db;


class M extends Base
{

	//查全部
    public function select($table)
    {   
        $data = Db::table($table)->select();
        return $data;
    }

    //查全部,根据条件
    public function selectWhere($table,$ziduan,$zhi)
    {   
        $data = Db::table($table)->where($ziduan,$zhi)->select();
        return $data;
    }

    //查单个,根据条件
    public function find($table,$ziduan,$zhi)
    {   
        $data = Db::table($table)->where($ziduan,$zhi)->find();
        return $data;
    }

    //添加返回id
    public function insertGetId($table,$data)
    {   
        $result = Db::table($table)->insertGetId($data);
        return $result;
    }

    //添加返回影响行数
    public function insert($table,$data)
    {   
        $result = Db::table($table)->insert($data);
        return $result;
    }

    //更新
    public function change($table,$id,$data)
    {   
        $result = Db::table($table)->where('id',$id)->update($data);
        return $result;
    }

    //灵活更新
    public function linghuo($table,$where,$data)
    {   
        $result = Db::table($table)->where($where)->update($data);
        return $result;
    }

    //删除
    public function del($table,$id)
    {   
        $result = Db::table($table)->where('id',$id)->delete();
        return $result;
    }
}