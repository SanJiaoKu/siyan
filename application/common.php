<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
function init_Password($password)
{
    $salt = '5^7*ban$V%^#@wM';
    return md5($password . $salt);
}

function setMsg($status, $message, $data = [], $httpCode = 200)
{
    $result = [
        'status' => $status,
        'message' => $message,
        'data' => $data
    ];
    return json($result, $httpCode);
}

function bootstrap_table_return($rows, $data)
{
    $return_data = array_values($data);
    $result = [
        'total' => $rows,
        'rows' => $return_data
    ];
    return json($result);
}

function billno()
{
    $ymCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    list($t1) = explode(' ', microtime());
    $micro_str = substr($t1, 2, 3);
    $billno = $ymCode[intval(date('Y')) - 2017] . $ymCode[intval(date('m'))] . date('dHis') . $micro_str . mt_rand(100, 999);
    return $billno;
}


function create_qrcode()
{
    vendor("phpqrcode.phpqrcode");
    $data = 'http://58bjcf.com/test.html?user=' . session('Auth.Username');
    $outfile = ROOT_PATH . "public/qrcode/" . session('Auth.Username') . '.jpg';
    $level = 'L';
    $size = 4;
    $QRcode = new \QRcode();
    ob_start();
    $QRcode->png($data, $outfile, $level, $size, 2);
    ob_end_clean();
    return time();
}

function CreateQrCode($url, $expend)
{
//    $expend = 'wang';
    vendor("phpQrcode.phpqrcode");
    header('Content-Type: image/png');
    $res = QRcode::png($url, 'broker/QrImg_' . $expend . '.png', 'L', 6);
    $logo = 'logo/logo.png';            //二维码中间logo图片
    $QR = 'broker/QrImg_' . $expend . '.png';          //原始二维码图
//    dump($res);die;
    if ($logo !== FALSE) {
        $logo = imagecreatefromstring(file_get_contents($logo));
        $QR = imagecreatefromstring(file_get_contents($QR));
        $QR_width = imagesx($QR);         //二维码图片宽度
        $QR_height = imagesy($QR);        //二维码图片高度
        $logo_width = imagesx($logo);
        $logo_height = imagesy($logo);
        $logo_qr_width = $QR_width / 7;
        $scale = $logo_width / $logo_qr_width;
        $logo_qr_height = $logo_height / $scale;
        $from_width = ($QR_width - $logo_qr_width) / 2;
        //重新组合图片并调整大小
        imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
    }
    //输出图片
    imagepng($QR, 'broker/QrImg_' . $expend . '.png');
    $qrcode = '/broker/QrImg_' . $expend . '.png';
    return $qrcode;
}

/**
 * @var 截取路由
 */
function getUrl($url)
{
    $pat = '/(http:\/\/[^\/]+\/)/';
    $url = preg_replace($pat, '', $url);
    return '/' . $url;
}

function newOrderNum($type = 0)
{
	if ($type) {
		$str = 'by';
	} else {
		$str = 'vp';
	}
	$ymCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
	list($t1) = explode(' ', microtime());
	$micro_str = substr($t1, 2, 3);
	$billno = $str . $ymCode[intval(date('Y')) - 2017] . $ymCode[intval(date('m'))] . date('dHis') . $micro_str . mt_rand(100, 999);
	return $billno;
}