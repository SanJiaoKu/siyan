<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/9/25
 * Time: 11:44
 */

namespace app\index\server;

use app\index\model\Message as MessageModel;
use think\Exception;

class Message extends BaseServer
{
	public function getList($id)
	{
		try {
			$list = MessageModel::where('uid', '=', $id)
				->alias('m')
				->where('is_read', 0)
				->join('sy_admin', 'sy_admin.id = m.admin_uid')
				->field('sy_admin.nickname, m.msg_contents')
				->select();
		} catch (Exception $e) {
			throw new DatabaseException([
				'errorCode' => '10001',
				'msg' => '非常抱歉,系统发生错误请联系管理员,谢谢您的配合'
			]);
		}
		return $list;
	}
}