<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/10/19
 * Time: 10:39
 */

namespace app\model;


class SignedModel extends BaseModel
{
	protected $table = 'sy_signed';

	protected $Length = 6;
}