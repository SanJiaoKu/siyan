<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/10/25
 * Time: 16:22
 */

namespace app\model;


use think\Db;

class CutDownModel extends BaseModel
{
    protected $table = 'sy_cutdown';

    protected $length = 6;

    /**
     * @var 获取砍价单号和个人信息
     */
    final public function getCut_User_Good($condition, $field)
    {
        try {
            $info = Db::name($this->table)
                ->alias('ct')
                ->join('sy_product_info pi', 'pi.id = ct.cut_pid')
                ->join('sy_user u', 'u.id = ct.cut_uid')
                ->where($condition)
                ->field($field)
                ->find();
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $info;
    }

}