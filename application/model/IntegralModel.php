<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/10/18
 * Time: 09:04
 */

namespace app\model;


class IntegralModel extends BaseModel
{
	protected $table = 'sy_integral_commodity';

	protected $length = 6;
}