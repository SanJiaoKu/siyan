<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/10/16
 * Time: 00:21
 */

namespace app\model;

use think\Db;
use think\cache\driver\Redis;
use app\index\controller\Signed;

class UserModel extends BaseModel
{
    protected $table = 'sy_user';

    protected $length = 6;

//	final public function signed($arr, $id)
//	{
//		Db::startTrans();
//		if ($arr['status'] === 0) {
//			try {
//				$insert = Db::name($this->check['joinTable'])->insert($arr);
//				// $update = self::where("id = {$id}")->update(['signed' => time(), 'intergral' => 'intergral' + $arr['number']]);
//				// 签到
////			$sql = "update " . $this->table . " set `intergral`= `intergral`+ {$arr['number']}, `signed` = unix_timestamp(now())  where `id` =  {$id}";
//				// 签到送钱活动
//				$sql = "update " . $this->table . " set `intergral`= `intergral`+ {$arr['number']}, `balance` =  `balance` + {$arr['number']},`signed` = unix_timestamp(now())  where `id` =  {$id}";
//				$result = Db::execute($sql);
//				Db::commit();
//			} catch (\Exception $e) {
//				Db::rollback();
//				throw (new \Exception('service error', 500));
//			}
//		} else {
//			try {
//				$insert = Db::name($this->check['joinTable'])->insert($arr);
//				// $update = self::where("id = {$id}")->update(['signed' => time(), 'intergral' => 'intergral' + $arr['number']]);
//				// 签到
////			$sql = "update " . $this->table . " set `intergral`= `intergral`+ {$arr['number']}, `signed` = unix_timestamp(now())  where `id` =  {$id}";
//				// 签到送钱活动
//				$sql = "update " . $this->table . " set `intergral`= `intergral`+ {$arr['number']}, `signed` = unix_timestamp(now())  where `id` =  {$id}";
//				$result = Db::execute($sql);
//				Db::commit();
//			} catch (\Exception $e) {
//				Db::rollback();
//				throw (new \Exception('service error', 500));
//			}
//		}
//
//
//		return $result;
//	}
    final public function signed($arr, $id)
    {
        Db::startTrans();
        try {
            Db::name($this->check['joinTable'])->insert($arr);
            // $update = self::where("id = {$id}")->update(['signed' => time(), 'intergral' => 'intergral' + $arr['number']]);
            if ($arr['status'] === 0) {
                // 签到送钱活动
                $sql = "update " . $this->table . " set `intergral`= `intergral`+ {$arr['number']}, `balance` =  `balance` + {$arr['number']},`signed` = unix_timestamp(now())  where `id` =  {$id}";
            } else {
                // 签到
                $sql = "update " . $this->table . " set `intergral`= `intergral`+ {$arr['number']}, `signed` = unix_timestamp(now())  where `id` =  {$id}";
            }
            $result = Db::execute($sql);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw (new \Exception('service error', 500));
        }
        return $result;
    }

    /**
     * @var 成为vip
     */
    final public function beVip($data, $uid, $orderNum)
    {
        Db::startTrans();
        try {
            // 新增充值vip记录
            Db::name('sy_recharge_member')->insert($data);
            // 改变用户状态
            Db::name('sy_user')->where("id = {$uid}")->update(['is_vip' => 1]);
            // 新增充值会员积分
            Db::name('sy_signed' . substr($uid, 1, -1))->insertGetId(['number' => 99, 'status' => 0, 'uid' => $uid, 'signed_time' => time(), 'sign_time' => date('Y-m-d', time())]);
            // 加上会员积分
            $sql = "update `sy_user` set `intergral`= `intergral`+ 99, `experience` = `experience` +  99, `signed` = unix_timestamp(now())  where `id` =  {$uid}";
            Db::execute($sql);
            // 上级ID
            $pid = Db::name('sy_user')->where("id = {$uid}")->value('p_id');
            if ($pid) {
                // 上上级ID
                $pPid = Db::name('sy_user')->where("id = {$pid}")->value('p_id');
            }
            // 上级加钱
            if ($pid) {
                $sql = 'UPDATE `sy_user` SET `balance` = `balance` + ' . $data['money'] * 0.7 . ' where id = ' . $pid;
                Db::execute($sql);
                // 上上级加钱
                if (!empty($pPid)) {
                    $sql = 'UPDATE `sy_user` SET `balance` = `balance` + ' . $data['money'] * 0.2 . ' where id = ' . $pPid;
                    Db::execute($sql);
                }
            }
            $username = Db::name('sy_user')->where('id', $uid)->value('username');
            Db::name('sy_user_daybook')->insertGetId(['billno' => $orderNum, 'income' => 99, 'uid' => $uid, 'description' => $username . '成为会员,支付:99元']);
            (new Redis())->rm($orderNum);
            // 提交事务
            Db::commit();
        } catch (\Error $e) {
            //回滚事务 记录日志
            Log::error($e);
            (new Redis())->set('error' . $orderNum, (new Redis())->get($orderNum));
            Db::rollback();
            return false;
        }
        return true;
    }
}