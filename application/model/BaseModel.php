<?php
/**
 * Created by PhpStorm.
 * User: 任旭明 <846723340@qq.com>
 * Date: 2018/10/15
 * Time: 23:51
 */

namespace app\model;

use think\Db;
use think\Model;

class BaseModel extends Model
{
    protected $table;

    protected $length;

    protected $check = [
        'order'     => '',
        'offset'    => '',
        'length'    => '',
        'joinTable' => [],
        'joinWhere' => '',
        'join'      => '',
        'as'        => '',
    ];

    /**
     * @var suffix order limit join as
     */
    public function __construct($suffix = '', $order = '', $limit = '', $joinTable = '', $joinWhere = '', $join = '', $as = '')
    {
        parent::__construct();
        if (is_numeric($suffix)) {
            $this->table .= '_' . $suffix;
        } elseif ($suffix) {
            $this->table = $suffix;
        }
        if ($order) {
            $this->check['order'] = $order;
        }
        if (is_numeric($limit) && $this->length) {
            $this->check['offset'] = $this->length * (int)$limit;
            $this->check['length'] = $this->length;
        }
        if ($as) {
            $this->check['as'] = $as;
        }
        if ($joinTable && $joinWhere) {
            $this->check['joinTable'] = $joinTable;
            $this->check['joinWhere'] = $joinWhere;
            $this->check['join'] = $join;
        }
    }

    /**
     * @var 取值
     */
    public function getValue($condition, $field)
    {
        try {
            $value = Db::name($this->table)
                ->alias($this->check['as'])
                ->where($condition)
                ->join($this->check['joinTable'], $this->check['joinWhere'], $this->check['join'])
                // ->fetchSql(true)
                ->value($field);
            // dump($value);die;
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $value;
    }

    /**
     * @var 统计
     */
    public function countNum($condition, $field)
    {
        try {
            $count = Db::name($this->table)
                ->alias($this->check['as'])
                ->where($condition)
                ->join($this->check['joinTable'], $this->check['joinWhere'], $this->check['join'])
                ->count($field);
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $count;
    }

    /**
     * @var 单条
     */
    public function findOne($condition, $field)
    {
        try {
            $info = Db::name($this->table)
                ->alias($this->check['as'])
                ->where($condition)
                ->join($this->check['joinTable'], $this->check['joinWhere'], $this->check['join'])
                ->field($field)
                ->find();
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $info;
    }

    /**
     * @var 集合
     */
    public function selectAll($condition, $field)
    {
        try {
            $list = Db::name($this->table)
                ->alias($this->check['as'])
                ->where($condition)
                ->join($this->check['joinTable'], $this->check['joinWhere'], $this->check['join'])
                ->limit($this->check['offset'], $this->check['length'])
                ->order($this->check['order'])
                ->field($field)
                ->select();
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $list;
    }

    /**
     * @var 修改
     */
    public function updateOne($condition, $arr)
    {
        try {
            $result = Db::name($this->table)
                ->where($condition)
                ->update($arr);
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $result;
    }

    /**
     * @var 添加
     */
    public function insertOne($arr)
    {
        try {
            $result = Db::name($this->table)
                ->insertGetId($arr);
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $result;
    }

    /**
     * @var 删除
     */
    public function deleteOne($id)
    {
        try {
            $result = Db::name($this->table)
                ->delete($id);
        } catch (\Exception $e) {
            throw (new \Exception('service error', 500));
        }
        return $result;
    }
}
